#!/bin/bash
#############################################################################
#     Date: 25/09/2019 11:26                                                #
#   Author: ALTencir Silva                                                  #
#    About: updating OI COB PF TV                                           #
#############################################################################

# loading environment variables #
source ~/.bash_profile

# path #
_PATH="/var/www/m2b_billings/oi/pf/tv/"

# acessing #
cd /var/www/m2b_billings/oi/pf/tv/

# each...
for _month in 06 07 08 09;
do
        # downloading file #
        echo "downloading files month: $_month ..."
        aws s3 cp s3://m2b-cobranca/oi/pf/tv/risco/2019/$_month/ . --recursive --exclude "*" --include "CARGA_CONTAX_OITV_2019${_month}??.zip"

        # unziping #
        echo "Extracting..."
        unzip '*.zip'

        # removing file #
        rm -f *.zip

        # replacing tab with space in all *.txt
        sed -i $'s/\t/ /g' *.txt

        # reading full folder #
        for _filename in `find ~/m2b_billings/oi/pf/tv/CARGA_CONTAX_OITV_*.txt -type f`
        do
                echo "Processing ${_filename}..."

                # find filename in files #
                _file_id=`psql -t -U postgres -d m2b -c "select id from oicob_pftv.files where file_name='$_filename';" | tr -d ' '`

                # found record in <files> #
                if [ "$_file_id" != "NULL" ] && [ "$_file_id" != "null" ] && [ "$_file_id" != "" ];
                then
                        # already been imported #
                        echo "Arquivo [$_filename] já foi importado!"
                        continue
                fi

                echo "Cleaning ${_filename}..."

                # setting enconding #
                iconv -c -f utf-8 -t ascii $_filename > "$_filename".tmp
                mv -f "$_filename".tmp "$_filename"
                rm -f "$_filename".tmp

                # setting return carriage
                sed -i 's/\r//g' $_filename

                # getting the lines count #
                _num_of_lines=$(cat "$_filename" | wc -l)

                echo "Importing ${_filename} to DB..."

                _product_id=14
                _filenumber=`date +%Y%m%d -d "$_days day ago"`
                _filedate=`date +%Y-%m-%d -d "$_days day ago"`
                _date=`date +%Y-%m-%d -d "0 day ago"`

                # adding new record in <files> #
                _file_id=`psql -t -q -U postgres -d m2b -c "insert into oicob_pftv.files(product_id,file_number,file_date,file_name,lines,created_at, processed)values($_product_id,$_filenumber,'$_filedate','$_filename',$_num_of_lines,'$_date','TRUE') returning id;" | tr -d ' '`

                # adding tmp #
                psql -U postgres -d m2b -c "truncate table oicob_pftv.__tmp__;"
                psql -U postgres -d m2b -c "\copy oicob_pftv.__tmp__ from '$_filename';"
                        
                # removing file #
                rm $_filename
                
                # adding new risks #
                echo "Adding risks..."
                psql -U postgres -d m2b -c "select * from oicob_pftv.initial_risk_sp();"
                #
        done
        #
done
#
echo "Done!"

