#!/bin/bash
#############################################################################
#     Date: 23/09/2019 11:26                                                #
#   Author: ALTencir Silva                                                  #
#    About: import of mailings OI COB PF TV                                 #
#############################################################################

# loading environment variables #
source ~/.bash_profile

# path #
_PATH="/var/www/m2b_billings/oi/pf/tv/"

# acessing #
cd /var/www/m2b_billings/oi/pf/tv/

# initializing variables #
_is_mailing="true"
_product_id=14

# each...
for _prefix_file in CARGA_CONTAX_OITV_ OITV_VOICER_M2B_;
do
        # extension #
        _extension="txt"
        _tmp="__tmp__"
        _days=1

        if [ "$_prefix_file" == "OITV_VOICER_M2B_" ];
        then
                _extension="TXT"
                _tmp="_tmp_"
                _days=1
        fi

        # variables #
        _date=`date +%Y-%m-%d -d "0 day ago"`
        _filedate=`date +%Y-%m-%d -d "$_days day ago"`
        _filenumber=`date +%Y%m%d -d "$_days day ago"`
        _filename_zip="${_prefix_file}${_filenumber}.zip"
        _filename="${_prefix_file}${_filenumber}.${_extension}"
        _filename_save="${_filename}.sav"

        # find filename in files #
        _file_id=`psql -t -U postgres -d m2b -c "select id from oicob_pftv.files where file_name='$_filename';" | tr -d ' '`

        # found record in <files> #
        if [ "$_file_id" != "NULL" ] && [ "$_file_id" != "null" ] && [ "$_file_id" != "" ];
        then
                # already been imported #
                echo "Arquivo [$_filename_zip] já foi importado!"
                continue
        fi

        # download mailing / risks #
        echo "Download ${_filename}..."

        if [ "$_prefix_file" == "OITV_VOICER_M2B_" ];
        then
                wget --ftp-user=$FTP_USER --ftp-password=$FTP_PASSWD ftp://$FTP_HOST/CONTAX/Keyrus/Moby2Buy/${_filename_zip}
        else
                wget --ftp-user=$FTP_USER_R1_PF --ftp-password=$FTP_PASSWD_R1_PF ftp://$FTP_HOST_R1_PF/1_Envio/COBRANCA/TV/${_filename_zip}
        fi

        # file not exists .zip? #
        if [ ! -f "$_filename_zip" ];
        then
                echo "Arquivo [$_filename_zip] ainda não disponibilizado no FTP."
                continue
        fi

        ##################################################################:> Uploading to AWS
        # aws s3 cp ${_filename_zip} s3://m2b-cobranca/oi/pf/tv/mailings/
        ##################################################################

        # unziping #
        echo "Extracting ${_filename}..."
        unzip ${_filename_zip}

        unrar e ${_filename_zip}

        # file not exists .txt? #
        if [ ! -f "$_filename" ];
        then
                echo "Erro na operação de unzip. Arquivo [$_filename] não encontrado!"
                exit
        fi

        echo "Cleaning ${_filename}..."

        # setting enconding #
        iconv -c -f utf-8 -t ascii $_filename > "$_filename".tmp
        mv -f "$_filename".tmp "$_filename"
        rm -f "$_filename".tmp

        # setting return carriage
        sed -i 's/\r//g' $_filename

        # replacing tab with space in $_filename
        sed -i $'s/\t/ /g' $_filename

        # getting the lines count #
        _num_of_lines=$(cat "$_filename" | wc -l)

        # found record in <files> #
        if [ "$_file_id" = "NULL" ] || [ "$_file_id" = "null" ] || [ "$_file_id" = "" ];
        then
                echo "Importing ${_filename} to DB..."

                # adding new record in <files> #
                _file_id=`psql -t -q -U postgres -d m2b -c "insert into oicob_pftv.files(product_id,file_number,file_date,file_name,lines,created_at, processed)values($_product_id,$_filenumber,'$_filedate','$_filename',$_num_of_lines,'$_date','TRUE') returning id;" | tr -d ' '`

                # adding new column in mailing csv [file_id] #
                if [ "$_prefix_file" == "OITV_VOICER_M2B_" ];
                then
                        awk -v d="$_file_id" -F";" 'BEGIN {OFS = ";"} FNR==1{$(NF+1)="file_id"} FNR>1{$(NF+1)=d;} 1' $_filename > $_filename_save 
                else
                        awk -v d="$_file_id" -F"|" 'BEGIN {OFS = "|"} FNR==1{$(NF+1)="file_id"} FNR>1{$(NF+1)=d;} 1' $_filename > $_filename_save
                fi

                # adding tmp #
                psql -U postgres -d m2b -c "truncate table oicob_pftv.$_tmp;"
                psql -U postgres -d m2b -c "\copy oicob_pftv.$_tmp from '$_filename_save';"
                
        else
                # has imported #
                echo "Arquivo [$_filename_zip] já foi importado!"
        fi

        # removing file #
        rm $_filename_zip
        rm $_filename

done

# if not error #
if [ $? -eq 0 ]; then

        echo "Preparing tables..."

        # preparing table #
        psql -U postgres -d m2b -c "TRUNCATE TABLE oicob_pftv._invoices RESTART IDENTITY CASCADE;"
        psql -U postgres -d m2b -c "TRUNCATE TABLE oicob_pftv._invoice_payments RESTART IDENTITY CASCADE;"

        # saving <invoices> #
        echo "Saving files [invoices / phones]..."
        _error=`psql -t -U postgres -d m2b -c "select * from oicob_pftv.invoices_sp($_is_mailing, $_file_id, $_filenumber, $_num_of_lines);" | tr -d ' '`

        # removing file #
        rm $_filename_save

        # update RISK #
        psql -U postgres -d m2b -c "select * from oicob_pftv.update_risk_sp();"
        #
else
        # removing file #
        rm $_filename_save

        # error import #
        echo "error importing..."
        # cat eligible/desktop.ini | mail -s "Cobrança OI-PF TV: <Erro na importação [Encoding] do Mailing>" altencir@mobi2buy.com, squad.cobranca@mobi2buy.com

        # rollback #
        # psql -U postgres -d m2b -c "delete from oicob_pftv.files where id=$_file_id;"
        #
fi 

#
echo "Done!"

