#!/bin/bash
#############################################################################
#     Date: 27/08/2019 18:05                                                #
#   Author: ALTencir Silva                                                  #
#    About: BOM/OI - Billing Omnichannel Mobi2buy - OI                      #
#############################################################################

# loading environment variables #
source ~/.bash_profile

# path #
_PATH="/var/www/m2b_billings/oi/pf/movel/"

# acessing #
cd /var/www/m2b_billings/oi/pf/movel/

# date #
_date_provider=`date +%Y-%m-%d -d "1 day ago"`

# reading platform ##########################################################
for _platform in 5717 6255;
do
  #
  _file_velip="Agrupa_${_platform}_data_${_date_provider}*.csv"

  # Downloading #
  echo "Downloading velip..."
  wget --ftp-user=$VELIP_USER --ftp-password=$VELIP_PASS ftp://$VELIP_HOST:$VELIP_PORT/relatorios/resultados_id${_platform}/processados/${_file_velip}
  wget --ftp-user=$VELIP_USER --ftp-password=$VELIP_PASS ftp://$VELIP_HOST:$VELIP_PORT/relatorios/resultados_id${_platform}/${_file_velip}

  # reading full folder #
  for _filename in `find ~/m2b_billings/oi/pf/movel/${_file_velip} -type f`
  do
    #
    echo "reading $_filename..."

    # encoding to UTF8 #
    iconv -c -f utf-8 -t ascii $_filename > "$_filename".tmp
    mv -f "$_filename".tmp "$_filename"

    # processing hello #
    psql -U postgres -d m2b -c "truncate table oicob_pfmovelpos._tmp_;"
    psql -U postgres -d m2b -c "\copy oicob_pfmovelpos._tmp_ from '$_filename';"
    psql -U postgres -d m2b -c "select * from oicob_pfmovelpos.ura_sp('$_filename', $_platform);"

    # removing file of provider #
    rm $_filename
    #
  done
  #
done

# processing iSMS responses #############################################################
echo "importing iSMS responses..."
bash ~/m2b_billings/oi/pf/movel/isms_read.sh

# importing #############################################################
echo "importing mailing..."
bash ~/m2b_billings/oi/pf/movel/import.sh

# payments #############################################################
echo "importing payments..."
bash ~/m2b_billings/oi/pf/movel/payments.sh

# processing eligibles #############################################################
echo "processing eligibles..."
psql -U postgres -d m2b -c "select * from oicob_pfmovelpos.eligibles_sp();"

# sending to s3 #############################################################
echo "processing..."
psql -t -U postgres -d m2b -c "\copy (select _sms,_isms,_voicer,_voicerbot,_email,velip_file,velip_nome,velip_numero,velip_numero2,velip_numero3,velip_extra1,velip_extra2,velip_extra3,velip_extra4,velip_extra5,velip_extra6,velip_extra7,velip_extra8,velip_cod_cli,msisdn,delay,email from oicob_pfmovelpos.dialings) To 'bom_dialings_smartware.csv' With CSV HEADER DELIMITER ';';"

##################################################################:> Uploading to AWS
# echo "sending s3..."
# aws s3 cp bom_dialings_smartware.csv s3://m2b-cobranca/velip/eligibles/
########################################################################

# echo "sending email..."
# cat text.txt | mail -s "ATENÇÃO! O arquivo de Cobrança OI Omnichannel [bom_dialings_smartware.csv] foi enviado para o S3." squad.cobranca@mobi2buy.com

# end #
echo "Done."
