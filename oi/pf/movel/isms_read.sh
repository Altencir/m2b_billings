#!/bin/bash
#############################################################################
#     Date: 05/09/2019 14:47                                                #
#   Author: ALTencir Silva                                                  #
#    About: BOM/OI - Save SMS Interactive                                   #
#############################################################################
# loading environment variables #
source ~/.bash_profile

# looping #
mysql -u smartware smartware -e "select l.id, l.msisdn, l.message from campaigns c inner join campaign_customer_bases b on b.campaign_id = c.id and c.campaign_type = 9 inner join messages m on m.campaign_customer_base_id = b.id inner join message_logs l on l.message_id = m.id and l.id > 1;" | while read _id _msisdn _message;
do

  # getting eligible_id #
  _msisdn=${_msisdn:2:13}
  _msisdn=$((_msisdn))

  _eligible_id=`psql -t -U postgres -d m2b -c "select id from oicob_pfmovelpos.eligibles where msisdn = $_msisdn ;" | tr -d ' '`

  echo "getting eligible_id...: [$_msisdn,($_eligible_id)]"

  if [ "$_eligible_id" != '' ]; then

    # writing isms #
    echo "adding isms: [$_id, $_msisdn, $_message, $_eligible_id]"
    psql -U postgres -d m2b -c "select * from oicob_pfmovelpos.isms_responses_sp($_id, $_msisdn, '$_message');"
    #

  fi
  
  #
done

# end #
echo "Done."
