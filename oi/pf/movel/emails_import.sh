#!/bin/bash
#############################################################################
#     Date: 05/09/2019 15:23                                                #
#   Author: ALTencir Silva                                                  #
#    About: BOM/OI - Export emails                                          #
#############################################################################
# loading environment variables #
source ~/.bash_profile

# downloading file #
echo "downloading files"
aws s3 cp s3://m2b-cobranca/dump/ . --recursive --exclude "*" --include "0000_part_??"

# reading full folder #
for _file in `find ~/m2b_billings/oi/pf/movel/0000* -type f`
do
  # inserting record #
  echo "adding email: [$_file]"
  psql -U postgres -d m2b -c "\copy public.enrich_emails(cpf_cnpj, email) FROM '$_file' DELIMITER '^' CSV;"
  rm "$_file"
  #
done

# downloading file #
echo "downloading files"
aws s3 cp s3://m2b-cobranca/dump/ . --recursive --exclude "*" --include "0000_part_???"

# reading full folder #
for _file in `find ~/m2b_billings/oi/pf/movel/0000* -type f`
do
  # inserting record #
  echo "adding email: [$_file]"
  psql -U postgres -d m2b -c "\copy public.enrich_emails(cpf_cnpj, email) FROM '$_file' DELIMITER '^' CSV;"
  rm "$_file"
  #
done

# end #
echo "Done."
