#!/bin/bash
#############################################################################
#     Date: 18/09/2019 16:45                                                #
#   Author: ALTencir Silva                                                  #
#    About: importing Velip files                                           #
#############################################################################

# loading environment variables #
source ~/.bash_profile

# path #
_PATH="/var/www/m2b_billings/oi/pf/fibra/"

# acessing #
cd /var/www/m2b_billings/oi/pf/fibra/

# temp: last 15 days #

for (( _day=15; _day>0; _day-- ))
do
  # date #
  _date_provider=`date +%Y-%m-%d -d "$_day day ago"`

  # reading platform ##########################################################
  for _platform in 5717;
  do
    #
    _file_velip="Agrupa_${_platform}_data_${_date_provider}*.csv"

    # Downloading #
    echo "Downloading velip..."
    wget --ftp-user=$VELIP_USER --ftp-password=$VELIP_PASS ftp://$VELIP_HOST:$VELIP_PORT/relatorios/resultados_id${_platform}/processados/${_file_velip}
    wget --ftp-user=$VELIP_USER --ftp-password=$VELIP_PASS ftp://$VELIP_HOST:$VELIP_PORT/relatorios/resultados_id${_platform}/${_file_velip}

    # reading full folder #
    for _filename in `find ~/m2b_billings/oi/pf/fibra/${_file_velip} -type f`
    do
      #
      echo "reading $_platform:$_filename..."

      # encoding to UTF8 #
      iconv -c -f utf-8 -t ascii $_filename > "$_filename".tmp
      mv -f "$_filename".tmp "$_filename"

      # processing hello #
      psql -U postgres -d m2b -c "truncate table oicob_pffibra._tmp_;"
      psql -U postgres -d m2b -c "\copy oicob_pffibra._tmp_ from '$_filename';"
      psql -U postgres -d m2b -c "select * from oicob_pffibra.uras_sp('$_filename', $_platform);"

      # removing file of provider #
      rm $_filename
      #
    done
    #
  done
  #
done