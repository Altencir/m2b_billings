CREATE OR REPLACE FUNCTION oicob_pjmovel.eligibles_sp()
 RETURNS character varying
 LANGUAGE plpgsql
AS $function$

declare _lines int8;
		_send_id int8;
		_rec record;
begin
	
	/* 
	 * Objetivo: definir msisdns elegíveis
	 * Data: 12/09/2019 10:47
	 * 
	 * */
	
	-- dialing_outs --
	insert into oicob_pjmovel.dialing_outs( debit_number, number_enrichment, reason )
	
	select msisdn, number_enrichment, 'N.MAI' 
	from oicob_pjmovel."_dialings" e
	where not exists
	(
		select i.id 
		from oicob_pjmovel."_invoices" i 
		where i.msisdn = e.msisdn
	);
	
-------------------------------------------------------------------------------------------------------

	insert into oicob_pjmovel.dialing_outs( debit_number, number_enrichment, reason )
	
	select msisdn, number_enrichment, 'N.ELE' 
	from oicob_pjmovel."_dialings" e
	where not exists 
	(
		select msisdn
		from
		(
			select 
				'RULE_'||cast(rule as varchar(3))||'_'||file||'_'||to_char(now(),'yyyyMMddHHMISS')||'#'||
				to_char(now(),'ddMMyyyy')||'#'||to_char(now(),'ddMMyyyy')||'#'||
				CASE
				WHEN extract(isodow from now()) NOT IN (0,6) AND SUBSTRING(CAST(debit_number AS CHAR(11)),1,2) IN ('21', '22', '24') THEN -- RJ --
					'0900#1900.CSV'
				WHEN extract(isodow from now()) NOT IN (0,6) AND SUBSTRING(CAST(debit_number AS CHAR(11)),1,2) IN ('84', '85', '88') THEN -- RN/CE --
					'0900#1800.CSV'
				WHEN extract(isodow from now()) NOT IN (0,6) AND SUBSTRING(CAST(debit_number AS CHAR(11)),1,2) NOT IN ('21','22','24','84','85','88') THEN -- is saturday --
					'0900#2100.CSV'
				WHEN extract(isodow from now()) = 6 AND SUBSTRING(CAST(debit_number AS CHAR(11)),1,2) NOT IN ('21','22','24','84','85','88') THEN -- is saturday --
					'1000#1600.CSV'
				ELSE
					'#'
				END 	 
				velip_file,
				 
				substring(name,1,10) velip_nome, 
				debit_number velip_numero, debit_number velip_numero2, debit_number velip_numero3,
				to_char(maturity,'dd/MM/yyyy') velip_extra1, 
				cast(_amount_invoice as varchar(2))||'#'||cast(value as varchar(12))||'#'||cast(_total_debt as varchar(12)) velip_extra2, 
				'OI_COB_MOVEL_PJ#TODOS#'||cast(arbor as varchar(10)) velip_extra3, 
				'smartware.customer_id' velip_extra4, 
				 
				CASE
				WHEN extract(isodow from now()) = 1 then 'Segunda, dia '
				WHEN extract(isodow from now()) = 2 then 'Terça, dia '
				WHEN extract(isodow from now()) = 3 then 'Quarta, dia '
				WHEN extract(isodow from now()) = 4 then 'Quinta, dia '
				WHEN extract(isodow from now()) = 5 then 'Sexta, dia '
				WHEN extract(isodow from now()) = 6 then 'Sábado, dia '
				WHEN extract(isodow from now()) = 0 then 'Domingo, dia '
				end
				
				||to_char((now() + INTERVAL '5 DAY'),'dd/MM/yyyy') velip_extra5,
					
				regexp_replace(right(cast(debit_number as varchar(13)),4), E'(.)(?!$)', E'\\1 ', 'g') velip_extra6,
				'R$'||cast(value as varchar(12)) velip_extra7,
				to_char((maturity + INTERVAL '0 DAY'),'dd/MM') velip_extra8,
				 
				cast(msisdn as varchar(11))||'#'||cpf_cnpj||'|'||cast(range_group as varchar(2))||'#'||cast(range_rest as varchar(2)) velip_cod_cli,
				 
				campaign_id, client_id, product_id, range_min, range_max, delay, risk_description_b, 
				debit_number, msisdn, used_enrichment, 
				(select min(e.email) from public.enrich_emails e where e.cpf_cnpj=l1.cpf_cnpj) email, _name, cpf_cnpj, 
				kind_person, arbor, invoice, reference, barcode, maturity, value, _total_debt, 
				_amount_invoice, field_key, 1 _send_id
				
			from
			(
				select 
					r.sms, r.isms, r.voicer, r.voicerbot, r.email,
					campaign_id, client_id, l1.product_id, 
					cast(substring(file,1,position('_' in file)-1) as integer) range_min, 
					cast(substring(file,position('_' in file)+1,2) as integer) range_max,
					delay, l1.risk_description_b, r.range_group, r.range_rest,
					(delay > 50) used_enrichment, l1.id, l1.rule, l1.risk, r.risk_signal,
					
					case 
					when exists(select o.name from oicob_pjmovel.offenses o where o."name"=l1.name) then
						case 
						 	when (r.sms or r.isms) is true then 'CLIENTE OI'
						 	when r.email is true then 'OI'
						 	when r.voicer is true then 'OI'
					 	end
					else
						l1.name
					end as name,
					
					l1.msisdn, yyyy_mm_ref, group_control, blacklist,
					
					case 
					when delay < 50 and l1.debit_number is not null then 
						l1.debit_number
					else 
						l1.msisdn 
					end debit_number,
					
					null _email, _name, cpf_cnpj, kind_person, arbor, invoice, reference, barcode, 
					maturity, value, _total_debt, _amount_invoice, field_key, file
					
				from
				(
					select distinct on(i.cpf_cnpj) i.cpf_cnpj, i.maturity, i.arbor, i.value, i.msisdn, 
						(
							select max(m.msisdn) 
							from oicob_pjmovel.msisdns m 
							where m.cpf_cnpj = i.cpf_cnpj
						) debit_number,
						(
							select min(cast(r.range_min as varchar)||'_'||cast(r.range_max as varchar))
							from oicob_pjmovel.rules r
							where (trunc(cast(DATE_PART('day', now() - i.maturity) as int),0) 
							between r.range_min and r.range_max)
						) file,
						(
							select min(r.range_group)
							from oicob_pjmovel.rules r
							where (trunc(cast(DATE_PART('day', now() - i.maturity) as int),0) 
							between r.range_min and r.range_max)
						) range_group,
						(
							select min(r.range_rest)
							from oicob_pjmovel.rules r
							where (trunc(cast(DATE_PART('day', now() - i.maturity) as int),0) 
							between r.range_min and r.range_max)
						) range_rest,
						
						r."rule", r.range_min, r.range_max, r.risk_description risk, r.risk_signal, 
						i.yyyy_mm_ref, i.risk_description_b, i.group_control,
						trunc(cast(DATE_PART('day', now() - i.maturity) as int),0) as delay, 
						i.field_key, i.blacklist,
						
						case -- vazio ou números e caracter especial --
						when (trim(i.name) = '') or length(trim(REGEXP_REPLACE(i.name,'[[:alpha:]]','','g'))) > 0 then
							'CLIENTE OI'
						else
							case when position(' ' in i.name) > 1 then -- tem sobrenome --
								case when length(trim(substring(i.name,1, position(' ' in i.name)))) > 10 then
									'CLIENTE OI'
								else
									trim(substring(i.name,1, position(' ' in i.name)))
								end
							else -- não tem sobrenome
								i.name
							end
						end as name, 
						
						i.id, r.min_total_debt,
						(
							select sum(v.value) 
						 	from oicob_pjmovel."_invoices" v 
						 	where v.cpf_cnpj = i.cpf_cnpj
						 	  and v.payday is null
						) _total_debt,
						(
							select count(v.value) 
						 	from oicob_pjmovel."_invoices" v 
						 	where v.cpf_cnpj = i.cpf_cnpj
						 	  and v.payday is null
						) _amount_invoice,
						
						c.id campaign_id, 1 client_id, f.product_id, i.name _name, i.kind_person, 
						i."number" invoice, i.yyyy_mm_ref reference, i.barcode
						
					from oicob_pjmovel."_invoices" i
					inner join oicob_pjmovel.files f on f.id = i.file_id
					inner join oicob_pjmovel.rules r on r.product_id = f.product_id
					inner join oicob_pjmovel.campaigns c on c.rule_id = r.id
					where 
						(
							select sum(v.value) 
					  	   	from oicob_pjmovel."_invoices" v 
					  	   	where v.cpf_cnpj = i.cpf_cnpj 
					  	   	  and v.payday is null
					  	) >= r.min_total_debt
					  and i.payday is null
					order by i.cpf_cnpj, i.maturity, i.risk_description desc
					
				) l1						  
				
				inner join oicob_pjmovel.rules r on 
				r.range_min = cast(substring(file,1,position('_' in file)-1) as integer) and
				r.range_max = cast(substring(file,position('_' in file)+1,2) as integer)
				
			) l1
			where
			(
				risk_description_b = '' or
				risk_description_b = 'BAIXO' or
				( 
					risk_description_b = 'MEDIO' and 
					(
						delay between 10 and 22
					) 
				) or
				( 
					(
						risk_description_b = 'INDEFINIDO' or
						risk_description_b = 'ALTO' or
						risk_description_b = 'FPD'
					) and delay between 23 and 50
				)
			)
			and
			
			-- adicionando descansos ----
			not exists
			(
				select b.id 
			 	from oicob_pjmovel.billings b 
			 	where
			 	-- retirando ALÔS
				(
					l1.cpf_cnpj = b.cpf_cnpj and 
					trunc(cast(DATE_PART('day', now() - b.date_hello) as int),0) <= 15
				)
			)
					
		) l1
		
		where l1.msisdn = e.msisdn

	);
	
-------------------------------------------------------------------------------------------------------
	
	-- preparing eligibles --
	truncate table oicob_pjmovel."_dialings" RESTART IDENTITY CASCADE;

	-- processing collaborator_sp --
--	select * into _rec from oicob_pjmovel.collaborator_sp();

	-- processing group_control_sp for eligibles --
--	select * into _rec from oicob_pjmovel.group_control_sp();

	-- adding eligibles in the temp table --
	insert into oicob_pjmovel."_dialings"
	select 
			debit_number, msisdn, id, file, delay, rule, range_min, range_max, 
			risk, risk_signal, name, cpf_cnpj, invoice, yyyy_mm_ref, maturity,
			value, risk_description_b, group_control, blacklist, field_key, now(), 
			arbor, range_group
	from
	(
		select 
			r.sms, r.isms, r.voicer, r.voicerbot, r.email,
			campaign_id, client_id, l1.product_id, 
			cast(substring(file,1,position('_' in file)-1) as integer) range_min, 
			cast(substring(file,position('_' in file)+1,2) as integer) range_max,
			delay, l1.risk_description_b, r.range_group, r.range_rest,
			(delay > 50) used_enrichment, l1.id, r.rule, l1.risk, r.risk_signal,
			
			case 
			when exists(select o.name from oicob_pjmovel.offenses o where o."name"=l1.name) then
				case 
				 	when (r.sms or r.isms) is true then 'CLIENTE OI'
				 	when r.email is true then 'OI'
				 	when r.voicer is true then 'OI'
			 	end
			else
				l1.name
			end as name,
			
			l1.msisdn, yyyy_mm_ref, group_control, blacklist,
			
			case 
			when delay < 50 and l1.debit_number is not null then 
				l1.debit_number
			else 
				l1.msisdn 
			end debit_number,
			
			null _email, _name, cpf_cnpj, kind_person, arbor, invoice, reference, barcode, 
			maturity, value, _total_debt, _amount_invoice, field_key, file
			
		from
		(
			select distinct on(i.cpf_cnpj) i.cpf_cnpj, i.maturity, i.arbor, i.value, i.msisdn, 
				(
					select max(m.msisdn) 
					from oicob_pjmovel.msisdns m 
					where m.cpf_cnpj = i.cpf_cnpj
				) debit_number,
				(
					select min(cast(r.range_min as varchar)||'_'||cast(r.range_max as varchar))
					from oicob_pjmovel.rules r
					where (trunc(cast(DATE_PART('day', now() - i.maturity) as int),0) 
					between r.range_min and r.range_max)
				) file,
				(
					select min(r.range_group)
					from oicob_pjmovel.rules r
					where (trunc(cast(DATE_PART('day', now() - i.maturity) as int),0) 
					between r.range_min and r.range_max)
				) range_group,
				(
					select min(r.range_rest)
					from oicob_pjmovel.rules r
					where (trunc(cast(DATE_PART('day', now() - i.maturity) as int),0) 
					between r.range_min and r.range_max)
				) range_rest,
				
				r."rule", r.range_min, r.range_max, r.risk_description risk, r.risk_signal, 
				i.yyyy_mm_ref, i.risk_description_b, i.group_control,
				trunc(cast(DATE_PART('day', now() - i.maturity) as int),0) as delay, 
				i.field_key, i.blacklist,
				
				case -- vazio ou números e caracter especial --
				when (trim(i.name) = '') or length(trim(REGEXP_REPLACE(i.name,'[[:alpha:]]','','g'))) > 0 then
					'CLIENTE OI'
				else
					case when position(' ' in i.name) > 1 then -- tem sobrenome --
						case when length(trim(substring(i.name,1, position(' ' in i.name)))) > 10 then
							'CLIENTE OI'
						else
							trim(substring(i.name,1, position(' ' in i.name)))
						end
					else -- não tem sobrenome
						i.name
					end
				end as name, 
				
				i.id, r.min_total_debt,
				(
					select sum(v.value) 
				 	from oicob_pjmovel."_invoices" v 
				 	where v.cpf_cnpj = i.cpf_cnpj
				 	  and v.payday is null
				) _total_debt,
				(
					select count(v.value) 
				 	from oicob_pjmovel."_invoices" v 
				 	where v.cpf_cnpj = i.cpf_cnpj 
				 	  and v.payday is null
				) _amount_invoice,
				
				c.id campaign_id, 1 client_id, f.product_id, i.name _name, i.kind_person, 
				i."number" invoice, i.yyyy_mm_ref reference, i.barcode
				
			from oicob_pjmovel."_invoices" i
			inner join oicob_pjmovel.files f on f.id = i.file_id
			inner join oicob_pjmovel.rules r on r.product_id = f.product_id
			inner join oicob_pjmovel.campaigns c on c.rule_id = r.id
			where 
				(
					select sum(v.value) 
			  	   	from oicob_pjmovel."_invoices" v 
			  	   	where v.cpf_cnpj = i.cpf_cnpj 
			  	   	  and v.payday is null
			  	) >= r.min_total_debt
			  and i.payday is null
			order by i.cpf_cnpj, i.maturity, i.risk_description desc
			
		) l1						  
		
		inner join oicob_pjmovel.rules r on 
		r.range_min = cast(substring(file,1,position('_' in file)-1) as integer) and
		r.range_max = cast(substring(file,position('_' in file)+1,2) as integer)
		
	) l1
	where
	(
		risk_description_b = '' or
		risk_description_b = 'BAIXO' or
		( 
			risk_description_b = 'MEDIO' and 
			(
				delay between 10 and 22
			) 
		) or
		( 
			(
				risk_description_b = 'INDEFINIDO' or
				risk_description_b = 'ALTO' or
				risk_description_b = 'FPD'
			) and delay between 23 and 50
		)
	)
	and
	
	-- adicionando descansos ----
	not exists
	(
		select b.id 
	 	from oicob_pjmovel.billings b 
	 	where
	 	-- retirando ALÔS
		(
			l1.cpf_cnpj = b.cpf_cnpj and 
			trunc(cast(DATE_PART('day', now() - b.date_hello) as int),0) <= 15
		)
	);

-------------------------------------------------------------------------------------------------------

	-- get lines --
	select count(1) into _lines from oicob_pjmovel."_dialings";

-------------------------------------------------------------------------------------------------------

	-- adding file send --
	insert into oicob_pjmovel.file_sends(name, campaign_id, lines, status)
	values('oicob_pjmovel_dialings_'||to_char(now(),'yyyyMMddHHMISS'), 1, _lines, true)
	returning id into _send_id;

-------------------------------------------------------------------------------------------------------

	-- adding eligibles in the definitive table --
	insert into oicob_pjmovel.dialings 
	(
		_sms, _isms, _voicer, _voicerbot, _email, range_rest,
		velip_file, velip_nome, velip_numero, velip_numero2, velip_numero3,
		velip_extra1, velip_extra2, velip_extra3, velip_extra4, 
		velip_extra5, velip_extra6, velip_extra7, velip_extra8, velip_cod_cli,
		 
		campaign_id, client_id, product_id, range_min, range_max, delay, risk, 
		msisdn, debit_number, used_enrichment, email, name, cpf_cnpj, 
		kind_person, arbor, invoice, reference, barcode, maturity, value, total_debt, 
		amount_invoice, fieldkey, file_send_id, range_group, gc_approach
	)
	 
	select 
		 (sms or isms), isms, voicer, voicerbot, l1.email, range_rest, 
		 'RULE_'||cast(rule as varchar(3))||'_'||file||'_'||to_char(now(),'yyyyMMddHHMISS')||'#'||
		 to_char(now(),'ddMMyyyy')||'#'||to_char(now(),'ddMMyyyy')||'#'||
			CASE
			WHEN extract(isodow from now()) NOT IN (0,6) AND SUBSTRING(CAST(debit_number AS CHAR(11)),1,2) IN ('21', '22', '24') THEN -- RJ --
				'0900#1900.CSV'
			WHEN extract(isodow from now()) NOT IN (0,6) AND SUBSTRING(CAST(debit_number AS CHAR(11)),1,2) IN ('84', '85', '88') THEN -- RN/CE --
				'0900#1800.CSV'
			WHEN extract(isodow from now()) NOT IN (0,6) AND SUBSTRING(CAST(debit_number AS CHAR(11)),1,2) NOT IN ('21','22','24','84','85','88') THEN -- is saturday --
				'0900#2100.CSV'
			WHEN extract(isodow from now()) = 6 AND SUBSTRING(CAST(debit_number AS CHAR(11)),1,2) NOT IN ('21','22','24','84','85','88') THEN -- is saturday --
				'1000#1600.CSV'
			ELSE
				'#'
			END 	 
		 velip_file,
		 
		 substring(name,1,10) velip_nome,
		 
		 case
		 when velip_numero is not null then
		 	velip_numero
		 when velip_numero2 is not null then
		 	velip_numero2
		 when velip_numero3 is not null then
		 	velip_numero3
		 end velip_numero, 
		 
		 case 
		 when (velip_numero2 is not null) then
		 	case 
		 	when (velip_numero2 != velip_numero) then
		 		velip_numero2
		 	when (velip_numero2 != velip_numero3) then 
		 		velip_numero3 
		 	else 
		 		null 
		 	end
		 when (velip_numero != velip_numero3) then
		 	velip_numero3
		 else
		 	null
		 end velip_numero2, 
		 
		 case 
		 when (velip_numero3 is not null) then
		 	case 
		 	when (velip_numero2 is null) or (velip_numero2 = velip_numero) then
		 		null
		 	when (velip_numero3 != velip_numero2) then
		 		velip_numero3
		 	else 
		 		null 
		 	end
		 else
		 	null
		 end velip_numero3, 
		 
		 to_char(maturity,'dd/MM/yyyy') velip_extra1, 
		 cast(_amount_invoice as varchar(2))||'#'||cast(value as varchar(12))||'#'||cast(_total_debt as varchar(12)) velip_extra2, 
		 'OI_COB_MOVEL_PJ#TODOS#'||cast(arbor as varchar(10)) velip_extra3, 
		 'smartware.customer_id' velip_extra4, 
		 
		 CASE
		 WHEN extract(isodow from now()) = 1 then 'Segunda, dia '
		 WHEN extract(isodow from now()) = 2 then 'Terça, dia '
		 WHEN extract(isodow from now()) = 3 then 'Quarta, dia '
		 WHEN extract(isodow from now()) = 4 then 'Quinta, dia '
		 WHEN extract(isodow from now()) = 5 then 'Sexta, dia '
		 WHEN extract(isodow from now()) = 6 then 'Sábado, dia '
		 WHEN extract(isodow from now()) = 0 then 'Domingo, dia '
		 END
		 ||to_char((now() + INTERVAL '5 DAY'),'dd/MM/yyyy') velip_extra5,
			
		 regexp_replace(right(cast(debit_number as varchar(13)),4), E'(.)(?!$)', E'\\1 ', 'g') velip_extra6,
		 'R$'||cast(value as varchar(12)) velip_extra7,
		 to_char((maturity + INTERVAL '0 DAY'),'dd/MM') velip_extra8,
		 
		 cast(msisdn as varchar(11))||'#'||cpf_cnpj||'|'||cast(range_group as varchar(2))||'#'||cast(range_rest as varchar(2)) velip_cod_cli,
		 
		 campaign_id, client_id, product_id, range_min, range_max, delay, risk_description_b, 
		 debit_number, msisdn, used_enrichment,  
		 (select min(e.email) from public.enrich_emails e where e.cpf_cnpj=l1.cpf_cnpj) email, substring(_name,1,60), cpf_cnpj, 
		 kind_person, arbor, invoice, reference, barcode, maturity, value, _total_debt, 
		 _amount_invoice, field_key, _send_id, range_group, 
		 
		 -- group control piloto --
		 true
		
	from
	(
		select 
			r.sms, r.isms, r.voicer, r.voicerbot, r.email,
			campaign_id, client_id, l1.product_id, 
			cast(substring(file,1,position('_' in file)-1) as integer) range_min, 
			cast(substring(file,position('_' in file)+1,2) as integer) range_max,
			delay, l1.risk_description_b, r.range_group, r.range_rest,
			(delay > 50) used_enrichment, l1.id, r.rule, l1.risk, r.risk_signal,
			
			case 
			when exists(select o.name from oicob_pjmovel.offenses o where o."name"=l1.name) then
				case 
				 	when (r.sms or r.isms) is true then 'CLIENTE OI'
				 	when r.email is true then 'OI'
				 	when r.voicer is true then 'OI'
			 	end
			else
				l1.name
			end as name,
			
			l1.msisdn, yyyy_mm_ref, group_control, blacklist,
			
			case 
			when delay < 50 and l1.debit_number is not null then 
				l1.debit_number
			else 
				l1.msisdn 
			end debit_number,
			
			debit1, debit2, debit3, msisdn1, msisdn2, msisdn3,

			case 
			when delay <= 50 then
				case when debit1 is not null then debit1 else msisdn1 end
			else
				case when msisdn1 is not null then msisdn1 else debit1 end
			end velip_numero, 
			
			case 
			when delay <= 50 then
			 	case when debit2 is not null then debit2 else msisdn2 end
			else
			 	case when msisdn2 is not null then msisdn2 else debit2 end
			end velip_numero2, 
			 
			case 
			when delay <= 50 then
				case when debit3 is not null then debit3 else msisdn3 end
			else
				case when msisdn3 is not null then msisdn3 else debit3 end
			end velip_numero3, 
			
			null _email, _name, cpf_cnpj, kind_person, arbor, invoice, reference, barcode, 
			maturity, value, _total_debt, _amount_invoice, field_key, file
			
		from
		(
			select distinct on(i.cpf_cnpj) i.cpf_cnpj, i.maturity, i.arbor, i.value, i.msisdn, 
				(
					select max(m.msisdn) 
					from oicob_pjmovel.msisdns m 
					where m.cpf_cnpj = i.cpf_cnpj
				) debit_number,
				(
					select min(cast(r.range_min as varchar)||'_'||cast(r.range_max as varchar))
					from oicob_pjmovel.rules r
					where (trunc(cast(DATE_PART('day', now() - i.maturity) as int),0) 
					between r.range_min and r.range_max)
				) file,
				(
					select min(r.range_group)
					from oicob_pjmovel.rules r
					where (trunc(cast(DATE_PART('day', now() - i.maturity) as int),0) 
					between r.range_min and r.range_max)
				) range_group,
				(
					select min(r.range_rest)
					from oicob_pjmovel.rules r
					where (trunc(cast(DATE_PART('day', now() - i.maturity) as int),0) 
					between r.range_min and r.range_max)
				) range_rest,
				
				r."rule", r.range_min, r.range_max, r.risk_description risk, r.risk_signal, 
				i.yyyy_mm_ref, i.risk_description_b, i.group_control,
				trunc(cast(DATE_PART('day', now() - i.maturity) as int),0) as delay, 
				i.field_key, i.blacklist,
				
				case -- vazio ou números e caracter especial --
				when (trim(i.name) = '') or length(trim(REGEXP_REPLACE(i.name,'[[:alpha:]]','','g'))) > 0 then
					'CLIENTE OI'
				else
					case when position(' ' in i.name) > 1 then -- tem sobrenome --
						case when length(trim(substring(i.name,1, position(' ' in i.name)))) > 10 then
							'CLIENTE OI'
						else
							trim(substring(i.name,1, position(' ' in i.name)))
						end
					else -- não tem sobrenome
						i.name
					end
				end as name, 
				
				i.id, r.min_total_debt,
				(
					select sum(v.value) 
				 	from oicob_pjmovel."_invoices" v 
				 	where v.cpf_cnpj = i.cpf_cnpj 
				 	  and v.payday is null
				) _total_debt,
				(
					select count(v.value) 
				 	from oicob_pjmovel."_invoices" v 
				 	where v.cpf_cnpj = i.cpf_cnpj 
				 	  and v.payday is null
				) _amount_invoice,
				
				c.id campaign_id, 1 client_id, f.product_id, i.name _name, i.kind_person, 
				i."number" invoice, i.yyyy_mm_ref reference, i.barcode,

				(select max(m.msisdn) from oicob_pjmovel.msisdns m where m.cpf_cnpj = i.cpf_cnpj and m.nr=1) debit1,
				(select max(m.msisdn) from oicob_pjmovel.msisdns m where m.cpf_cnpj = i.cpf_cnpj and m.nr=2) debit2,
				(select max(m.msisdn) from oicob_pjmovel.msisdns m where m.cpf_cnpj = i.cpf_cnpj and m.nr=3) debit3,
				
				(select max(m.msisdn) from oicob_pjmovel."_invoice_phones" m where m.cpf_cnpj = i.cpf_cnpj and m.nr=1 and m.is_contact is true) msisdn1,
				(select max(m.msisdn) from oicob_pjmovel."_invoice_phones" m where m.cpf_cnpj = i.cpf_cnpj and m.nr=2 and m.is_contact is true) msisdn2,
				(select max(m.msisdn) from oicob_pjmovel."_invoice_phones" m where m.cpf_cnpj = i.cpf_cnpj and m.nr=3 and m.is_contact is true) msisdn3
				
			from oicob_pjmovel."_invoices" i
			inner join oicob_pjmovel.files f on f.id = i.file_id
			inner join oicob_pjmovel.rules r on r.product_id = f.product_id
			inner join oicob_pjmovel.campaigns c on c.rule_id = r.id
			where 
				(
					select sum(v.value) 
			  	   	from oicob_pjmovel."_invoices" v 
			  	   where v.cpf_cnpj = i.cpf_cnpj 
			  	     and v.payday is null
			  	) >= r.min_total_debt
			and i.payday is null
			order by i.cpf_cnpj, i.maturity, i.risk_description desc
			
		) l1						  
		
		inner join oicob_pjmovel.rules r on 
		r.range_min = cast(substring(file,1,position('_' in file)-1) as integer) and
		r.range_max = cast(substring(file,position('_' in file)+1,2) as integer)
		
	) l1
	where
	(
		risk_description_b = '' or
		risk_description_b = 'BAIXO' or
		( 
			risk_description_b = 'MEDIO' and 
			(
				delay between 10 and 22
			) 
		) or
		( 
			(
				risk_description_b = 'INDEFINIDO' or
				risk_description_b = 'ALTO' or
				risk_description_b = 'FPD'
			) and delay between 23 and 50
		)
	)
	and
	
	-- adicionando descansos ----
	not exists
	(
		select b.id 
	 	from oicob_pjmovel.billings b 
	 	where
	 	-- retirando ALÔS
		(
			l1.cpf_cnpj = b.cpf_cnpj and 
			trunc(cast(DATE_PART('day', now() - b.date_hello) as int),0) <= 15
		)
	);

	-- fechando --
    RETURN 'sucesso!';
END;
$function$
;

CREATE OR REPLACE FUNCTION oicob_pjmovel.invoices_sp(_is_mailing boolean, _file_id integer, _nr_file bigint, _ln bigint)
 RETURNS text
 LANGUAGE plpgsql
AS $function$
declare _value numeric(12,2);
		_valuec varchar(12);
		_rec record;
		_product_id integer := 11;
		_product_ids char(2) := '11';
		_ddd_msisdn_tam int4 := 11;
		_msisdn_tam int4 := 09;
		_cpf_cnpj_tam int4 := 11; 
		_invoice_id int8;
	
		-- ddd_telefone_fixo_1;ddd_telefone_fixo_2;ddd_telefone_movel_1;ddd_telefone_movel_2
		
		_phone varchar[] := array[	'ddd_telefone_fixo_1',
							 		'ddd_telefone_fixo_2',
							 		'ddd_telefone_movel_1',
							 		'ddd_telefone_movel_2' ];
		_t varchar;
		_msisdn int8;
		_filename varchar;
		_filedate date;
		_filenumber int8;
		_content varchar(1) := '';
	
		_file_mailing_id int8;
	
		_rejected text := '';
		_id int8;
		_fieldkey varchar(50);
begin
	
	/* Objetivo: importar faturas/invoices
	 * Data: 04/07/2019 15:23
	 * Key: arbor
	 * 
	 * */
	
	-- arquivo tem conteúdo --
	RAISE NOTICE 'p1: %, p2: %, p3: %, p4: %', _is_mailing, _file_id, _nr_file, _ln;

	if _ln > 0 then
	
		_filenumber := _nr_file;
	
		-- pode importar --
		RAISE NOTICE 'File Id: %', _file_id;
	
		if 	( 	_file_id is not null and 
				(
					not exists
					(
						select id 
		     		 	from oicob_pjmovel._invoices 
		     			where file_id = _file_id 
		     		 	limit 1
		     		 )
			     ) 
		    ) then
		
			-- mailing --
			RAISE NOTICE 'E mailing? %', _file_id;
		
			if _is_mailing then
				
				-- looping --
				for _rec in 
				
					select l2.*
					from
					(
						select -- arbor.maturity.fatura.dtfat.value --
							trim(informacao[06]) ||'.'|| trim(informacao[08]) ||'.'|| trim(informacao[11])
							||'.'|| trim(informacao[13]) ||'.'|| trim(informacao[12]) as field_key, 
							trim(informacao[01]) as name,
							'' as email,
							trim(informacao[02]) as cpf_cnpj,
							trim(informacao[03]) as kind_person,
							trim(informacao[39]) as msisdn,
							trim(informacao[39]) as fixed1,
							trim(informacao[40]) as fixed2,
							trim(informacao[41]) as mobile1,
							trim(informacao[42]) as mobile2,
							'' as yyyy_mm_ref,
							trim(informacao[13]) as maturity,
							trim(informacao[12]) as value,
							'' as payday,
							trim(informacao[06]) as arbor,
							trim(informacao[32]) as barcode,
							'' as reason_return,
							trim(informacao[33]) as risk_description,
							'' as risk_number,
							true as active,
							now() as created_at,
							trim(informacao[11]) as number,
							trim(informacao[44]) as product_name,
							trim(informacao[45]) as product_group,
							case when trim(informacao[24]) = '' then '000' else replace(replace(trim(informacao[24]),',',''),'.','') end as vl_ori,
							case when trim(informacao[30]) = '' then '000' else replace(replace(trim(informacao[30]),',',''),'.','') end as vl_ter,
							case when trim(informacao[27]) = '' then '000' else replace(replace(trim(informacao[27]),',',''),'.','') end as vl_aju,
							trim(informacao[10]) as un,
							trim(informacao[17]) as isfree,
							trim(informacao[18]) as suspended,
							trim(informacao[11]) as invoice,
							trim(informacao[44]) as product,
							trim(informacao[04]) as plan
						from
						(
							select 
								row_number() OVER (ORDER BY content) AS nr,
								STRING_TO_ARRAY('NOME;CPF_CNPJ;TIPO_PESSOA;PLANO;UF;ARBOR;SIEBEL;DATA_MOTIVADORA;CENARIO;UNIDADE_NEGOCIO;FATURA;VL_FATURA;DT_FATURA;TEMPO_PLANTA;FAIXA_VALOR_ORIGINAL;FAIXA_VALOR_ATUAL;ISENTO;SUSPENSO;FAIXA_ATRASO;FAIXA_ATRASO_ORIGINAL;PRORROGACAO;AGENCIAS;METODO_PAGAMENTO;VALOR_ORIGINAL;VENC_ORIGINAL;DATA_AJUSTE;VALOR_AJUSTE;DATA_PAGAMENTO;VALOR_PAGAMENTO;VALOR_TERCEIROS;CONVERGENTE;CODIGO_DE_BARRA;CD_RISCO;NO_OFERTA_1;NO_OFERTA_2;NO_OFERTA_3;DDD_TERMINAL_CONTATO;CONTESTACAO;DDD_TELEFONE_1;DDD_TELEFONE_2;DDD_TELEFONE_3;DDD_TELEFONE_4;CD_IDENT_PESSOA;PRODUTO;CATEG_PRODUTO;OIT_POSSUI_MOVEL;TIPO_EVENTO_VENDA',';') as campo,
								STRING_TO_ARRAY(content,';') as informacao	 	   
							from oicob_pjmovel._tmp_
						) l1 
						where trim(informacao[01]) <> 'NOME' -- nome --
					) l2
					where 
					  kind_person = 'J'     -- tipo de pessoa --
					  and un = 'EMP'   -- unidade de negócio --
					  and trim(vl_ori) <> ''     -- valor original --
					
				loop
				
					_rec.msisdn := case when trim(_rec.msisdn) = '' then '0' else trim(_rec.msisdn) end;
				
								
					RAISE NOTICE 'Incluindo MSISDN: %', _rec.msisdn;
				
					_value :=
							(cast( substring(_rec.vl_ori,1,length(_rec.vl_ori)-2) ||'.'||	
								   substring(_rec.vl_ori,length(_rec.vl_ori)-1,2) as numeric(12,2) ) +     -- valor original --
					  		 cast( substring(_rec.vl_ter,1,length(_rec.vl_ter)-2) ||'.'||	
								   substring(_rec.vl_ter,length(_rec.vl_ter)-1,2) as numeric(12,2) )) -    -- valor terceiros --
					  		 cast( substring(_rec.vl_aju,1,length(_rec.vl_aju)-2) ||'.'||	
								   substring(_rec.vl_aju,length(_rec.vl_aju)-1,2) as numeric(12,2) );	   -- valor terceiros -- 
							
					_rec.maturity := substring(_rec.maturity,7,4) ||'-'|| substring(_rec.maturity,4,2) ||'-'|| substring(_rec.maturity,1,2);
				
					-- saving ------------------------------------------------------------------------------------------------------------------
					insert into oicob_pjmovel._invoices
					(file_id, field_key, number, arbor, "name", cpf_cnpj, kind_person, msisdn, yyyy_mm_ref, maturity, value, 
					 barcode, risk_description, risk_number, active, created_at, debit_number)
					values
					(_file_id, _rec.field_key, cast(_rec.number as int8), cast(_rec.arbor as int8), 
					 _rec.name, trim(_rec.cpf_cnpj), trim(_rec.kind_person), 
					 cast(_rec.msisdn as int8), '.', cast(_rec.maturity as date), _value, 
					 _rec.barcode, substring(_rec.risk_description,1,15), 0, true, now(), cast(_rec.msisdn as int8)) 
					returning id into _invoice_id;
					
					-- saving eligibles to funnel --
					insert into oicob_pjmovel.eligibles
						(file_id, field_key, "name", email, cpf_cnpj, kind_person, msisdn, yyyy_mm_ref, maturity, value, payday, barcode,
						 reason_return, risk_description, risk_number, "number", whatsapp, hello, group_control, blacklist, contributor,
						 enrich_1, enrich_2, risk_description_b, active, created_at, updated_at, arbor, debit_number)
					select 
						file_id, field_key, "name", email, cpf_cnpj, kind_person, msisdn, yyyy_mm_ref, maturity, value, payday, barcode,
						reason_return, risk_description, risk_number, "number", whatsapp, hello, group_control, blacklist, contributor,
						enrich_1, enrich_2, risk_description_b, active, created_at, updated_at, arbor, debit_number
					from oicob_pjmovel."_invoices" 
					where id = _invoice_id;
					
					-- adding phones debit -----------------------------------------------------------------------------------
					if trim(_rec.msisdn) <> '0' then
					
						insert into oicob_pjmovel."_invoice_phones"
						(file_id, invoice_id, msisdn, status, created_at, is_contact, field_key, cpf_cnpj, debit_number)
						values
						(_file_id, _invoice_id,	cast(_rec.msisdn as int8), true, now(), false, _rec.field_key, trim(_rec.cpf_cnpj),
						 cast(_rec.msisdn as int8));
						
					end if;
				
					-- adding fixed contact ---------------------------------------------------------------------------------
					if (trim(_rec.fixed1) <> '') and (trim(_rec.fixed1) <> trim(_rec.msisdn)) then
					
						insert into oicob_pjmovel."_invoice_phones"
						(file_id, invoice_id, msisdn, status, created_at, is_contact, field_key, cpf_cnpj, debit_number)
						values
						(_file_id, _invoice_id, cast(_rec.fixed1 as int8), true, now(), true, _rec.field_key, trim(_rec.cpf_cnpj),
						 cast(_rec.fixed1 as int8));
						
					end if;
				
					-- adding fixed contact ---------------------------------------------------------------------------------
					if (trim(_rec.fixed2) <> '') and (trim(_rec.fixed2) <> trim(_rec.msisdn)
						and (trim(_rec.fixed2) <> trim(_rec.fixed1))) then
					
						insert into oicob_pjmovel."_invoice_phones"
						(file_id, invoice_id, msisdn, status, created_at, is_contact, field_key, cpf_cnpj, debit_number)
						values
						(_file_id, _invoice_id, cast(_rec.fixed2 as int8), true, now(), true, _rec.field_key, trim(_rec.cpf_cnpj),
						 cast(_rec.fixed2 as int8));
					
					end if;
				
					-- adding mobile contact ---------------------------------------------------------------------------------
					if (trim(_rec.mobile1) <> '') and (trim(_rec.mobile1) <> trim(_rec.msisdn)
						and (trim(_rec.mobile1) <> trim(_rec.fixed1))
						and (trim(_rec.mobile1) <> trim(_rec.fixed2))) then
					
						insert into oicob_pjmovel."_invoice_phones"
						(file_id, invoice_id, msisdn, status, created_at, is_contact, field_key, cpf_cnpj, debit_number)
						values
						(_file_id, _invoice_id, cast(_rec.mobile1 as int8), true, now(), true, _rec.field_key, trim(_rec.cpf_cnpj),
						 cast(_rec.mobile1 as int8));
					end if;
				
					-- adding mobile contact ---------------------------------------------------------------------------------
					if (trim(_rec.mobile2) <> '') and (trim(_rec.mobile2) <> trim(_rec.msisdn)
						and (trim(_rec.mobile2) <> trim(_rec.fixed1))
						and (trim(_rec.mobile2) <> trim(_rec.fixed2))
						and (trim(_rec.mobile2) <> trim(_rec.mobile1))) then
					
						insert into oicob_pjmovel."_invoice_phones"
						(file_id, invoice_id, msisdn, status, created_at, is_contact, field_key, cpf_cnpj, debit_number)
						values
						(_file_id, _invoice_id, cast(_rec.mobile2 as int8), true, now(), true, _rec.field_key, trim(_rec.cpf_cnpj),
						 cast(_rec.mobile2 as int8));
					
					end if;
				
				
				end loop;
			
				RAISE NOTICE 'Enumerando telefones...';
				
				update oicob_pjmovel."_invoice_phones"
				set nr = p.nr
				from
				(
					select row_number() OVER (PARTITION BY p.cpf_cnpj) AS nr, p.cpf_cnpj, p.msisdn
					from oicob_pjmovel."_invoice_phones" p
					group by p.cpf_cnpj, p.msisdn
				) p
				where p.cpf_cnpj = oicob_pjmovel."_invoice_phones".cpf_cnpj and p.msisdn = oicob_pjmovel."_invoice_phones".msisdn;

				RAISE NOTICE '%', _rejected;
				
				return _rejected;
			
			else 

				-- is not mailing --
				if not exists(
								select id 
							  	from oicob_pjmovel._invoice_payments 
							  	where file_id = _file_id
							 ) then
				
					RAISE NOTICE 'Liquidando faturas...';
				
					-- payments for cta_arbor --
					
					for _rec in
					
						select tp_operacao, cta_arbor, nr_fatura, dt_vencimento
						from
						(
							select 
								campo[1] registro,
								campo[2] cta_arbor,
								campo[3] nr_fatura,
								campo[4] "sequence",
								campo[5] dt_vencimento,
								campo[6] vl_original,
								campo[7] vl_aberto,
								campo[8] vl_pago,
								campo[9] vl_ajustado,
								campo[10] tipo_fatura,
								campo[11] codigo_barra,
								campo[12] dt_pagamento,
								campo[13] dt_ajuste,
								campo[14] tp_operacao,
								campo[15] dt_devolucao
							from
							(
								-- id --
								SELECT 0 AS nr,	STRING_TO_ARRAY('registro,cta_arbor,nr_fatura,sequence,dt_vencimento,vl_original,vl_aberto,vl_pago,vl_ajustado,tipo_fatura,codigo_barra,dt_pagamento,dt_ajuste,motivo_devolucao,dt_devolucao',',') as campo
								
								union
								
								-- invoices --
								SELECT row_number() OVER (ORDER BY content) AS nr, STRING_TO_ARRAY(content,'||') as informacao	 	   
								from oicob_pjmovel._tmp_ 
								where substring(content,1,2) = '02'
								
							) l1
							where nr > 0
						) l2
						where tp_operacao in ('02','03','04','07')
						
					loop
					
						-- find msisdn -- 
						select msisdn into _msisdn from oicob_pjmovel."_invoices" where arbor = cast(_rec.cta_arbor as int8);
					
						if _msisdn is not null then
						
							-- add payments -----------------------
							insert into oicob_pjmovel.payments
							(file_id, fieldkey, msisdn, date_ref, reason, created_at)
							values
							(_file_id, _rec.cta_arbor, _msisdn, date(now()), _rec.tp_operacao, now()); 
	
							-- add _invoice_payments -----------------------
							insert into oicob_pjmovel."_invoice_payments"
							(file_id, field_key, date_ref, created_at)
							values
							(_file_id, _rec.cta_arbor, date(now()), now()); 
						
							-- updating _invoices --------------------------
							update oicob_pjmovel."_invoices"
							set payday = date(now())
							where arbor = cast(_rec.cta_arbor as int8);
						
						end if;
					
					end loop;

				else
				
					raise notice 'Arquivo já foi processado.';
				
				end if;
	 		
			end if;
	
		end if;
	
	end if;
	
    RETURN 'sucesso!';
END;
$function$
;

CREATE OR REPLACE FUNCTION oicob_pjmovel.update_risk_sp()
 RETURNS character varying
 LANGUAGE plpgsql
AS $function$

declare _rec record;
		_cpf_cnpj varchar(14) := '';
		_risk varchar(30) := '';
	
begin
	
	-- preparando --
	truncate table oicob_pjmovel._risks;
	
	-- ajustando RISCOS do mailing --
	for _rec in
		
		select cpf_cnpj, trim(risk_description) risk_description
		from oicob_pjmovel._invoices
		where cpf_cnpj in 
		(
			select cpf_cnpj 
			from oicob_pjmovel._invoices 
			group by cpf_cnpj
			having count(1) > 1
		)
		order by cpf_cnpj, risk_description desc
		
	loop
	
		-- cnpj diferente --
		if _cpf_cnpj != _rec.cpf_cnpj then
		
			_cpf_cnpj := _rec.cpf_cnpj;
			_risk := _rec.risk_description;
		
		else
			-- risco diferente --
			if _rec.risk_description != _risk then
			
				case
					when 'BAIXO' in (_rec.risk_description, _risk) then
						case 
							when '' in (_rec.risk_description, _risk) then
								_risk := 'BAIXO';
							when 'INDEFINIDO' in (_rec.risk_description, _risk) then
								_risk := 'INDEFINIDO';
							when 'MEDIO' in (_rec.risk_description, _risk) then
								_risk := 'MEDIO';
							when 'ALTO' in (_rec.risk_description, _risk) then
								_risk := 'ALTO';
							when 'FPD' in (_rec.risk_description, _risk) then
								_risk := 'FPD';
							else
								_risk := '';
						end case;
					when 'INDEFINIDO' in (_rec.risk_description, _risk) then
						case 
							when '' in (_rec.risk_description, _risk) then
								_risk := 'INDEFINIDO';
							when 'MEDIO' in (_rec.risk_description, _risk) then
								_risk := 'INDEFINIDO';
							when 'ALTO' in (_rec.risk_description, _risk) then
								_risk := 'ALTO';
							when 'FPD' in (_rec.risk_description, _risk) then
								_risk := 'FPD';
							else
								_risk := '';
						end case;
					when 'MEDIO' in (_rec.risk_description, _risk) then
						case 
							when '' in (_rec.risk_description, _risk) then
								_risk := 'MEDIO';
							when 'ALTO' in (_rec.risk_description, _risk) then
								_risk := 'ALTO';
							when 'FPD' in (_rec.risk_description, _risk) then
								_risk := 'FPD';
							else
								_risk := '';
						end case;
					when 'ALTO' in (_rec.risk_description, _risk) then
						case 
							when '' in (_rec.risk_description, _risk) then
								_risk := 'ALTO';
							when 'FPD' in (_rec.risk_description, _risk) then
								_risk := 'FPD';
							else
								_risk := '';
						end case;
					when 'FPD' in (_rec.risk_description, _risk) then
						if '' in (_rec.risk_description, _risk) then
							_risk := 'FPD';
						end if;
					when _rec.risk_description || _risk = '' then
						_risk := 'INDEFINIDO';
				end case;
			
				-- saving --
				insert into oicob_pjmovel._risks (cpf_cnpj, risk) 
				values (_cpf_cnpj, _risk);
			
			else
			
				if trim(_rec.risk_description) || trim(_risk) = '' then
				
					_risk := 'INDEFINIDO';
				
					-- saving --
					insert into oicob_pjmovel._risks (cpf_cnpj, risk) 
					values (_cpf_cnpj, _risk);
				
				end if;
			
			end if;
		
		end if;
	
	end loop;

	-- atualizando _invoices --
	update oicob_pjmovel._invoices
	set risk_description_b = r.risk
	from oicob_pjmovel._risks r
	where r.cpf_cnpj = oicob_pjmovel._invoices.cpf_cnpj;

	update oicob_pjmovel._invoices
	set risk_description_b=case when coalesce(risk_description,'')='' then 'INDEFINIDO' else risk_description end
	where coalesce(risk_description_b,'') = ''; 

	-- fim --
	return 'Sucesso!';

END;
$function$
;

CREATE OR REPLACE FUNCTION oicob_pjmovel.uras_sp(_filename character varying, _platform integer)
 RETURNS character varying
 LANGUAGE plpgsql
AS $function$


DECLARE
   _ret varchar;
   _reg record;
   _lin integer;
   _file_send_id integer;
   _file_return_id integer;
   _file_type_id integer;
   _ura_id integer;
   _eligible_id int8;
   _status_id integer;
   _campaign_id integer;
   _provider_id integer;
   _voice_return_type_number bool;
   _msisdn varchar(20) := '';
   _debit_number int8;
   _cpf_cnpj varchar(14);
   _call_datetime_funnel varchar(19);
   _x varchar;
   _i int4;
   _range_group integer;
   _range_rest integer;
   
   -- platforms velip --
   _f5717 varchar := 'N;cd_id;cd_date;cd_time;cd_time_start;cd_time_end;cd_time_sec;cd_time_sec2;cd_price;cd_value;cd_name;cd_route;cd_called_status;cd_called_status2;ctid;cpid;cp_id;cd_resp1;cd_resp2;cd_resp3;cd_resp4;cd_resp5;cd_resp6;cd_resp7;cd_resp8;cd_dest;cdc_id;cdc_nome;cdc_extra1;cdc_extra2;cdc_extra3;cdc_extra4;cdc_extra5;cdc_extra6;cdc_extra7;cdc_extra8;cdc_cod_cli;tipo;cp_ctid;cf_tentativa';
   _f6255 varchar := 'N;cd_id;cd_date;cd_time;cd_time_start;cd_time_end;cd_time_sec;cd_time_sec2;cd_price;cd_value;cd_name;cd_route;cd_called_status;cd_called_status2;ctid;cpid;cp_id;cd_resp1;cd_resp2;cd_resp3;cd_resp4;cd_resp5;cd_resp6;cd_resp7;cd_resp8;cd_resp9;cd_resp10;cd_resp11;cd_resp12;cd_resp13;cd_resp14;cd_resp15;cd_resp16;cd_resp17;cd_dest;cdc_id;cdc_nome;cdc_extra1;cdc_extra2;cdc_extra3;cdc_extra4;cdc_extra5;cdc_extra6;cdc_extra7;cdc_extra8;cdc_cod_cli;tipo;cp_ctid;cf_tentativa';
  
BEGIN
	begin
		
		/*
		 * ALT: 23/08/2019 17:00
		 * -----------------------------------------------------------------------
		 * Objetivo: processar arquivo retorno VELIP
		 * -----------------------------------------------------------------------
		 */
		
		RAISE NOTICE 'Verificando arquivo:[%], plataforma:[%]...', _filename, _platform;
	
		-- is VELIP? --
		select count(1) into _lin
		from
		(
			SELECT row_number() OVER (ORDER BY content) AS nr,
			STRING_TO_ARRAY((case when _platform = 5717 then _f5717 else _f6255 end), ';') as campo,
			STRING_TO_ARRAY(content,';') as informacao	 	   
			from oicob_pjmovel._tmp_
		) l1 where informacao[25] = 'cd_resp8';
	
		if _lin is not null and _lin > 0  then -- is VELIP --

			-- pegando number lines VELIP --
			select count(1) into _lin
			from
			(
				SELECT row_number() OVER (ORDER BY content) AS nr,
				STRING_TO_ARRAY((case when _platform = 5717 then _f5717 else _f6255 end), ';') as campo,
				STRING_TO_ARRAY(content,';') as informacao	 	   
				from oicob_pjmovel._tmp_
			) l1 where informacao[25] <> 'cd_resp8';
	
			-- checando se o arquivo é mesmo VELIP --
			if _lin is not null and _lin > 1  then
			
				RAISE NOTICE 'Arquivo VELIP com % linhas', _lin;
			
				-- já importou este arquivo? --
				select id 
				into _file_return_id 
				from oicob_pjmovel.file_returns 
				where name = _filename;
			
				if _file_return_id is null then
				
					-- salvando file_returns pegando o ID --
					insert into oicob_pjmovel.file_returns
					(name, campaign_id, lines, return_processed, status, created_at)
					values
					(_filename, 1, _lin, true,	true, now()) returning id into _file_return_id;

					-- loop salvando billings --
					for _reg in
					
						select * 
						from(
							select nr,
								1 campaign_id,
								1 client_id,
								7 product_id,
								_platform platform,
								informacao[(case when _platform = 5717 then 28 else 37 end)] "name",
								informacao[(case when _platform = 5717 then 26 else 35 end)] msisdn,
								false used_enrichment,
								informacao[3] || ' ' || informacao[4] call_datetime,
								informacao[7]  call_duration,
								informacao[(case when _platform = 5717 then 40 else 49 end)] call_attempts,
								informacao[13] call_status,
								informacao[14] call_status2,
								informacao[18] cd_resp1,
								informacao[19] cd_resp2,
								informacao[20] cd_resp3,
								informacao[21] cd_resp4,
								informacao[22] cd_resp5,
								informacao[23] cd_resp6,
								informacao[24] cd_resp7,
								informacao[25] cd_resp8,
								case when _platform = 5717 then '' else informacao[26] end cd_resp9,
								case when _platform = 5717 then '' else informacao[27] end cd_resp10,
								case when _platform = 5717 then '' else informacao[28] end cd_resp11,
								case when _platform = 5717 then '' else informacao[29] end cd_resp12,
								case when _platform = 5717 then '' else informacao[30] end cd_resp13,
								case when _platform = 5717 then '' else informacao[31] end cd_resp14,
								case when _platform = 5717 then '' else informacao[32] end cd_resp15,
								case when _platform = 5717 then '' else informacao[33] end cd_resp16,
								case when _platform = 5717 then '' else informacao[34] end cd_resp17,
								informacao[(case when _platform = 5717 then 29 else 38 end)] extra1,
								informacao[(case when _platform = 5717 then 30 else 39 end)] extra2,
								informacao[(case when _platform = 5717 then 31 else 40 end)] extra3,
								informacao[(case when _platform = 5717 then 32 else 41 end)] extra4,
								
								informacao[(case when _platform = 5717 then 33 else 42 end)] extra5,
								informacao[(case when _platform = 5717 then 34 else 43 end)] extra6,
								informacao[(case when _platform = 5717 then 35 else 44 end)] extra7,
								informacao[(case when _platform = 5717 then 36 else 45 end)] extra8,
								
								informacao[(case when _platform = 5717 then 37 else 46 end)] codcli,
								informacao[(case when _platform = 5717 then 39 else 48 end)] file_name
							from
							(
							
								SELECT row_number() OVER (ORDER BY content) AS nr,
								STRING_TO_ARRAY((case when _platform = 5717 then _f5717 else _f6255 end), ';') as campo,
								STRING_TO_ARRAY(content,';') as informacao	 	   
								from oicob_pjmovel._tmp_
								
							) l1 where informacao[25] <> 'cd_resp8') l2
							
						where (strpos(extra3, '#MVE#') > 0 
							or strpos(file_name, 'RULE_100_') > 0)
							
						order by file_name, msisdn, call_datetime desc
						
					loop
						
						RAISE NOTICE 'MSISDN: %', _reg.msisdn;
					
						if (strpos(_reg.extra3, '#MVE#') > 0 or 
							strpos(_reg.file_name, 'RULE_100_') > 0)
						then
						
							if trim(_reg.call_status) <> '' then
							
								RAISE NOTICE 'Processando MSISDN: %', _reg.msisdn;
							
								-- msisdn --
								_msisdn := _reg.msisdn;
								
								RAISE NOTICE 'Salvando [file_record_returns]: %', _reg.msisdn;
							
								-- salvando em uras --
								
								insert into oicob_pjmovel.uras
									(campaign_id, client_id, product_id, platform, name, msisdn, call_datetime, 
									 call_duration, call_attempts, call_status, call_status2, cd_resp1, cd_resp2,
									 cd_resp3, cd_resp4, cd_resp5, cd_resp6, cd_resp7, cd_resp8, cd_resp9,
									 cd_resp10, cd_resp11, cd_resp12, cd_resp13, cd_resp14, cd_resp15, cd_resp16, 
									 cd_resp17, extra1, extra2, extra3, extra4, extra5, extra6, extra7, extra8, 
									 codcli, filename, file_return_id)
								values
									( _reg.campaign_id, _reg.client_id, _reg.product_id, _platform, substring(_reg.name,1,80), 
									cast(_reg.msisdn as int8), cast(_reg.call_datetime as timestamp), 
									case when trim(_reg.call_duration) = '' then 0 else cast(_reg.call_duration as integer) end,
									cast(_reg.call_attempts as integer), _reg.call_status, _reg.call_status2, 
									_reg.cd_resp1, _reg.cd_resp2, _reg.cd_resp3, _reg.cd_resp4, _reg.cd_resp5,
									_reg.cd_resp6, _reg.cd_resp7, _reg.cd_resp8, _reg.cd_resp9, _reg.cd_resp10,
									_reg.cd_resp11, _reg.cd_resp12, _reg.cd_resp13, _reg.cd_resp14, _reg.cd_resp15,
									_reg.cd_resp16, _reg.cd_resp17, _reg.extra1, _reg.extra2, _reg.extra3,
									_reg.extra4, _reg.extra5, _reg.extra6, _reg.extra7, _reg.extra8, _reg.codcli, 
									_reg.file_name, _file_return_id) returning id into _ura_id;
									
								RAISE NOTICE 'Salvando [billings]: %', _reg.msisdn;
								
								-- deu alô? --
								if _reg.call_status in ('1','OK') then
								
									_cpf_cnpj := substring(_reg.extra3, 1, position('#' in _reg.extra3)-1);
								
									-- salvando em billings --
									if exists(select id 
											  from oicob_pjmovel.billings 
											  where cpf_cnpj = _cpf_cnpj) then	
		
										update oicob_pjmovel.billings
										set date_hello = cast(_reg.call_datetime as date),
											updated_at = now()
										where cpf_cnpj = _cpf_cnpj;
									
									else
									
										insert into oicob_pjmovel.billings
										(msisdn, debit_number, cpf_cnpj, date_hello, filename, range_group, range_rest)
										values
										(cast(_reg.msisdn as int8), cast(_reg.msisdn as int8), _cpf_cnpj, cast(_reg.call_datetime as date), 
										_reg.file_name, _range_group, _range_rest);
									
									end if;
								
								end if;
							
							end if;
						end if;
					
					end loop;
				end if;
			
			elseif _lin = 1 then
			
				RAISE NOTICE 'Arquivo vazio!';
			
			end if;

		end if;
	
		RETURN 'Concluído!';
	
	exception when others then 
	
 		GET STACKED DIAGNOSTICS _ret = PG_EXCEPTION_CONTEXT;

		_ret := _ret || 'SQL ERROR  ' || SQLERRM || ' - ERROR OF SQL => ' || SQLSTATE;

    	return _ret;	
    
	end;
END
$function$
;
