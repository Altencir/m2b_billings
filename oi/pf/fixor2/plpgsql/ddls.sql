
-- Drop table

-- DROP TABLE oicob_pjmovel."_dialings";

CREATE TABLE oicob_pjmovel."_dialings" (
	number_enrichment int8 NULL,
	msisdn int8 NULL,
	id int8 NULL,
	file text NULL,
	delay numeric NULL,
	"rule" int4 NULL,
	range_min int4 NULL,
	range_max int4 NULL,
	risk varchar(15) NULL,
	risk_signal varchar(5) NULL,
	"name" text NULL,
	cpf_cnpj varchar(14) NULL,
	"number" int8 NULL,
	yyyy_mm_ref bpchar(7) NULL,
	maturity date NULL,
	value numeric(12,2) NULL,
	risk_description varchar(15) NULL,
	group_control date NULL,
	blacklist date NULL,
	field_key varchar(50) NULL,
	created_at timestamp NOT NULL DEFAULT now(),
	arbor int8 NOT NULL DEFAULT 0,
	range_group int4 NULL DEFAULT 0,
	range_rest int4 NULL DEFAULT 0
);
CREATE INDEX "_dialings_cpf_cnpj_IDX" ON oicob_pjmovel._dialings USING btree (cpf_cnpj);
CREATE INDEX "_dialings_delay_IDX" ON oicob_pjmovel._dialings USING btree (delay);
CREATE INDEX "_dialings_field_key_IDX" ON oicob_pjmovel._dialings USING btree (field_key);
CREATE INDEX "_dialings_maturity_IDX" ON oicob_pjmovel._dialings USING btree (maturity);
CREATE INDEX "_dialings_msisdn_IDX" ON oicob_pjmovel._dialings USING btree (msisdn);
CREATE INDEX "_dialings_number_enrichment_IDX" ON oicob_pjmovel._dialings USING btree (number_enrichment);
CREATE INDEX "_dialings_risk_description_IDX" ON oicob_pjmovel._dialings USING btree (risk_description);
CREATE INDEX "_dialings_rule_IDX" ON oicob_pjmovel._dialings USING btree (rule);
CREATE INDEX "_dialings_yyyy_mm_ref_IDX" ON oicob_pjmovel._dialings USING btree (yyyy_mm_ref);

-- Drop table

-- DROP TABLE oicob_pjmovel."_risks";

CREATE TABLE oicob_pjmovel."_risks" (
	cpf_cnpj varchar(14) NULL,
	risk varchar(30) NULL
);

-- Drop table

-- DROP TABLE oicob_pjmovel."_tmp_";

CREATE TABLE oicob_pjmovel."_tmp_" (
	"content" text NULL
);

-- Drop table

-- DROP TABLE oicob_pjmovel.billings;

CREATE TABLE oicob_pjmovel.billings (
	id int8 NOT NULL GENERATED ALWAYS AS IDENTITY,
	msisdn int8 NOT NULL,
	debit_number int8 NULL,
	cpf_cnpj varchar(14) NOT NULL,
	date_hello date NULL,
	filename varchar(80) NULL,
	range_group int4 NULL DEFAULT 0,
	range_rest int4 NULL DEFAULT 0,
	created_at timestamp NOT NULL DEFAULT now(),
	updated_at timestamp NOT NULL DEFAULT now(),
	CONSTRAINT billings_pkey PRIMARY KEY (id)
);
CREATE INDEX billings_cpf_cnpj_idx ON oicob_pjmovel.billings USING btree (cpf_cnpj);
CREATE INDEX billings_created_at_idx ON oicob_pjmovel.billings USING btree (created_at);
CREATE INDEX billings_date_hello_idx ON oicob_pjmovel.billings USING btree (date_hello);
CREATE INDEX billings_debit_number_idx ON oicob_pjmovel.billings USING btree (debit_number);
CREATE INDEX billings_filename_idx ON oicob_pjmovel.billings USING btree (filename);
CREATE INDEX billings_msisdn_idx ON oicob_pjmovel.billings USING btree (msisdn);
CREATE INDEX billings_range_group_idx ON oicob_pjmovel.billings USING btree (range_group);

-- Drop table

-- DROP TABLE oicob_pjmovel.dialing_outs;

CREATE TABLE oicob_pjmovel.dialing_outs (
	id int4 NOT NULL GENERATED ALWAYS AS IDENTITY,
	debit_number int8 NOT NULL,
	number_enrichment int8 NOT NULL,
	reason varchar(5) NULL,
	created_at timestamp NOT NULL DEFAULT now(),
	CONSTRAINT dialing_outs_pk PRIMARY KEY (id)
);
CREATE INDEX dialing_outs_created_at_idx ON oicob_pjmovel.dialing_outs USING btree (created_at);
CREATE INDEX dialing_outs_debit_number_idx ON oicob_pjmovel.dialing_outs USING btree (debit_number);
CREATE INDEX dialing_outs_number_enrichment_idx ON oicob_pjmovel.dialing_outs USING btree (number_enrichment);
CREATE INDEX dialing_outs_reason_idx ON oicob_pjmovel.dialing_outs USING btree (reason);

-- Drop table

-- DROP TABLE oicob_pjmovel.file_returns;

CREATE TABLE oicob_pjmovel.file_returns (
	id int4 NOT NULL GENERATED ALWAYS AS IDENTITY,
	"name" varchar(100) NOT NULL,
	campaign_id int4 NOT NULL,
	lines int4 NOT NULL,
	return_processed bool NOT NULL DEFAULT false,
	status bool NOT NULL DEFAULT false,
	created_at timestamp NOT NULL DEFAULT now(),
	updated_at timestamp NOT NULL DEFAULT now(),
	CONSTRAINT file_returns_pkey PRIMARY KEY (id)
);
CREATE INDEX file_returns_campaign_id_idx ON oicob_pjmovel.file_returns USING btree (campaign_id);
CREATE INDEX file_returns_created_at_idx ON oicob_pjmovel.file_returns USING btree (created_at);
CREATE INDEX file_returns_name_idx ON oicob_pjmovel.file_returns USING btree (name);
CREATE INDEX file_returns_return_processed_idx ON oicob_pjmovel.file_returns USING btree (return_processed);

-- Drop table

-- DROP TABLE oicob_pjmovel.file_sends;

CREATE TABLE oicob_pjmovel.file_sends (
	id int4 NOT NULL GENERATED ALWAYS AS IDENTITY,
	"name" varchar(100) NOT NULL,
	campaign_id int4 NOT NULL,
	lines int4 NOT NULL,
	status bool NOT NULL DEFAULT false,
	created_at timestamp NOT NULL DEFAULT now(),
	updated_at timestamp NOT NULL DEFAULT now(),
	CONSTRAINT file_sends_pkey PRIMARY KEY (id)
);
CREATE INDEX file_sends_campaign_id_idx ON oicob_pjmovel.file_sends USING btree (campaign_id);
CREATE INDEX file_sends_created_at_idx ON oicob_pjmovel.file_sends USING btree (created_at);
CREATE INDEX file_sends_name_idx ON oicob_pjmovel.file_sends USING btree (name);

-- Drop table

-- DROP TABLE oicob_pjmovel.msisdns;

CREATE TABLE oicob_pjmovel.msisdns (
	id int4 NOT NULL GENERATED ALWAYS AS IDENTITY,
	cpf_cnpj varchar(20) NULL,
	msisdn int8 NULL,
	created_at timestamp NOT NULL DEFAULT now(),
	nr int4 NOT NULL DEFAULT 0,
	CONSTRAINT msisdns_pkey PRIMARY KEY (id)
);
CREATE INDEX msisdns_cpf_cnpj_idx ON oicob_pjmovel.msisdns USING btree (cpf_cnpj);
CREATE INDEX msisdns_created_at_idx ON oicob_pjmovel.msisdns USING btree (created_at);
CREATE INDEX msisdns_msisdn_idx ON oicob_pjmovel.msisdns USING btree (msisdn);
CREATE INDEX msisdns_nr_idx ON oicob_pjmovel.msisdns USING btree (nr);

-- Drop table

-- DROP TABLE oicob_pjmovel.offenses;

CREATE TABLE oicob_pjmovel.offenses (
	id int4 NOT NULL GENERATED ALWAYS AS IDENTITY,
	"name" varchar(50) NOT NULL,
	created_at timestamp NOT NULL DEFAULT now(),
	CONSTRAINT offenses_pkey PRIMARY KEY (id)
);
CREATE INDEX offenses_created_at_idx ON oicob_pjmovel.offenses USING btree (created_at);
CREATE INDEX offenses_name_idx ON oicob_pjmovel.offenses USING btree (name);

-- Drop table

-- DROP TABLE oicob_pjmovel.dialings;

CREATE TABLE oicob_pjmovel.dialings (
	id int8 NOT NULL GENERATED ALWAYS AS IDENTITY,
	"_sms" bool NULL,
	"_isms" bool NULL,
	"_voicer" bool NULL,
	"_voicerbot" bool NULL,
	"_email" bool NULL,
	velip_file varchar(80) NULL,
	velip_nome varchar(50) NULL,
	velip_numero varchar(50) NULL,
	velip_numero2 varchar(50) NULL,
	velip_numero3 varchar(50) NULL,
	velip_extra1 varchar(50) NULL,
	velip_extra2 varchar(50) NULL,
	velip_extra3 varchar(50) NULL,
	velip_extra4 varchar(50) NULL,
	velip_extra5 varchar(50) NULL,
	velip_extra6 varchar(50) NULL,
	velip_extra7 varchar(50) NULL,
	velip_extra8 varchar(50) NULL,
	velip_cod_cli varchar(50) NULL,
	campaign_id int4 NOT NULL,
	client_id int4 NOT NULL,
	product_id int4 NOT NULL,
	range_min int4 NULL DEFAULT 0,
	range_max int4 NULL DEFAULT 0,
	range_group int4 NULL DEFAULT 0,
	range_rest int4 NULL DEFAULT 0,
	delay int4 NULL DEFAULT 0,
	risk varchar(15) NULL,
	msisdn int8 NOT NULL,
	debit_number int8 NULL,
	used_enrichment bool NOT NULL DEFAULT false,
	cpc int8 NULL,
	email varchar(100) NULL,
	"name" varchar(60) NULL,
	cpf_cnpj varchar(14) NULL DEFAULT NULL::character varying,
	kind_person bpchar(1) NULL DEFAULT 'F'::bpchar,
	arbor int8 NULL,
	invoice int8 NULL,
	reference varchar(7) NULL,
	barcode varchar(60) NULL,
	maturity date NULL,
	value numeric(12,2) NULL DEFAULT 0,
	total_debt numeric(12,2) NULL DEFAULT 0,
	amount_invoice int4 NOT NULL DEFAULT 0,
	gc_approach bool NULL DEFAULT false,
	fieldkey varchar(50) NULL,
	file_send_id int4 NULL,
	created_at timestamp NOT NULL DEFAULT now(),
	updated_at timestamp NOT NULL DEFAULT now(),
	CONSTRAINT dialings_pkey PRIMARY KEY (id),
	CONSTRAINT dialings_file_send_id_fkey FOREIGN KEY (file_send_id) REFERENCES oicob_pjmovel.file_sends(id)
);
CREATE INDEX dialings__email_idx ON oicob_pjmovel.dialings USING btree (_email);
CREATE INDEX dialings__isms_idx ON oicob_pjmovel.dialings USING btree (_isms);
CREATE INDEX dialings__sms_idx ON oicob_pjmovel.dialings USING btree (_sms);
CREATE INDEX dialings__voicer_idx ON oicob_pjmovel.dialings USING btree (_voicer);
CREATE INDEX dialings__voicerbot_idx ON oicob_pjmovel.dialings USING btree (_voicerbot);
CREATE INDEX dialings_arbor_idx ON oicob_pjmovel.dialings USING btree (arbor);
CREATE INDEX dialings_cpc_idx ON oicob_pjmovel.dialings USING btree (cpc);
CREATE INDEX dialings_cpf_cnpj_idx ON oicob_pjmovel.dialings USING btree (cpf_cnpj);
CREATE INDEX dialings_created_at_idx ON oicob_pjmovel.dialings USING btree (created_at);
CREATE INDEX dialings_debit_number_idx ON oicob_pjmovel.dialings USING btree (debit_number);
CREATE INDEX dialings_delay_idx ON oicob_pjmovel.dialings USING btree (delay);
CREATE INDEX dialings_email_idx ON oicob_pjmovel.dialings USING btree (email);
CREATE INDEX dialings_invoice_idx ON oicob_pjmovel.dialings USING btree (invoice);
CREATE INDEX dialings_msisdn_idx ON oicob_pjmovel.dialings USING btree (msisdn);
CREATE INDEX dialings_name_idx ON oicob_pjmovel.dialings USING btree (name);
CREATE INDEX dialings_range_group_idx ON oicob_pjmovel.dialings USING btree (range_group);
CREATE INDEX dialings_range_rest_idx ON oicob_pjmovel.dialings USING btree (range_rest);
CREATE INDEX dialings_risk_idx ON oicob_pjmovel.dialings USING btree (risk);

-- Drop table

-- DROP TABLE oicob_pjmovel.uras;

CREATE TABLE oicob_pjmovel.uras (
	id int8 NOT NULL GENERATED ALWAYS AS IDENTITY,
	campaign_id int4 NOT NULL,
	client_id int4 NOT NULL,
	product_id int4 NOT NULL,
	platform int8 NULL,
	"name" varchar(80) NULL,
	msisdn int8 NOT NULL,
	used_enrichment bool NOT NULL DEFAULT false,
	call_datetime timestamp NULL,
	call_duration int4 NULL,
	call_attempts int4 NULL,
	call_status bpchar(2) NULL,
	call_status2 bpchar(2) NULL,
	cd_resp1 varchar(50) NULL,
	cd_resp2 varchar(50) NULL,
	cd_resp3 varchar(50) NULL,
	cd_resp4 varchar(50) NULL,
	cd_resp5 varchar(50) NULL,
	cd_resp6 varchar(50) NULL,
	cd_resp7 varchar(50) NULL,
	cd_resp8 varchar(50) NULL,
	cd_resp9 varchar(50) NULL,
	cd_resp10 varchar(50) NULL,
	cd_resp11 varchar(50) NULL,
	cd_resp12 varchar(50) NULL,
	cd_resp13 varchar(50) NULL,
	cd_resp14 varchar(50) NULL,
	cd_resp15 varchar(50) NULL,
	cd_resp16 varchar(50) NULL,
	cd_resp17 varchar(50) NULL,
	extra1 varchar(60) NULL,
	extra2 varchar(60) NULL,
	extra3 varchar(60) NULL,
	extra4 varchar(60) NULL,
	extra5 varchar(60) NULL,
	extra6 varchar(60) NULL,
	extra7 varchar(60) NULL,
	extra8 varchar(60) NULL,
	codcli varchar(60) NULL,
	filename varchar(60) NULL,
	file_return_id int4 NULL,
	created_at timestamp NOT NULL DEFAULT now(),
	updated_at timestamp NOT NULL DEFAULT now(),
	CONSTRAINT uras_pkey PRIMARY KEY (id),
	CONSTRAINT uras_file_return_id_fkey FOREIGN KEY (file_return_id) REFERENCES oicob_pjmovel.file_returns(id)
);
CREATE INDEX uras_call_datetime_idx ON oicob_pjmovel.uras USING btree (call_datetime);
CREATE INDEX uras_created_at_idx ON oicob_pjmovel.uras USING btree (created_at);
CREATE INDEX uras_filename_idx ON oicob_pjmovel.uras USING btree (filename);
CREATE INDEX uras_msisdn_idx ON oicob_pjmovel.uras USING btree (msisdn);
CREATE INDEX uras_platform_idx ON oicob_pjmovel.uras USING btree (platform);
CREATE INDEX uras_used_enrichment_idx ON oicob_pjmovel.uras USING btree (used_enrichment);

-- Drop table

-- DROP TABLE oicob_pjmovel."_invoice_payments";

CREATE TABLE oicob_pjmovel."_invoice_payments" (
	id int8 NOT NULL GENERATED ALWAYS AS IDENTITY,
	file_id int4 NOT NULL,
	field_key varchar(50) NOT NULL,
	date_ref date NOT NULL,
	created_at timestamp NOT NULL DEFAULT now(),
	CONSTRAINT "_invoice_payments_pkey" PRIMARY KEY (id)
);
CREATE INDEX "_invoice_payments_created_at_IDX" ON oicob_pjmovel._invoice_payments USING btree (created_at);
CREATE INDEX "_invoice_payments_date_ref_IDX" ON oicob_pjmovel._invoice_payments USING btree (date_ref);
CREATE INDEX "_invoice_payments_field_key_IDX" ON oicob_pjmovel._invoice_payments USING btree (field_key);
CREATE INDEX "_invoice_payments_file_id_IDX" ON oicob_pjmovel._invoice_payments USING btree (file_id);

-- Drop table

-- DROP TABLE oicob_pjmovel."_invoice_phones";

CREATE TABLE oicob_pjmovel."_invoice_phones" (
	id int8 NOT NULL GENERATED ALWAYS AS IDENTITY,
	file_id int4 NOT NULL,
	invoice_id int8 NULL,
	msisdn int8 NULL,
	status bool NOT NULL DEFAULT true,
	is_contact bool NOT NULL DEFAULT false,
	field_key varchar(50) NULL,
	cpf_cnpj varchar(14) NULL,
	debit_number int8 NULL,
	created_at timestamp NOT NULL DEFAULT now(),
	updated_at timestamp NOT NULL DEFAULT now(),
	nr int4 NOT NULL DEFAULT 0,
	CONSTRAINT "_invoice_phones_pkey" PRIMARY KEY (id)
);
CREATE INDEX "_invoice_phones_cpf_cnpj_IDX" ON oicob_pjmovel._invoice_phones USING btree (cpf_cnpj);
CREATE INDEX "_invoice_phones_field_key_IDX" ON oicob_pjmovel._invoice_phones USING btree (field_key);
CREATE INDEX _invoice_phones_file_id_idx ON oicob_pjmovel._invoice_phones USING btree (file_id);
CREATE INDEX _invoice_phones_invoice_id_idx ON oicob_pjmovel._invoice_phones USING btree (invoice_id);
CREATE INDEX "_invoice_phones_is_contact_IDX" ON oicob_pjmovel._invoice_phones USING btree (is_contact);
CREATE INDEX _invoice_phones_msisdn_idx ON oicob_pjmovel._invoice_phones USING btree (msisdn);
CREATE INDEX "_invoice_phones_nr_IDX" ON oicob_pjmovel._invoice_phones USING btree (nr);
CREATE INDEX _invoice_phones_status_idx ON oicob_pjmovel._invoice_phones USING btree (status);

-- Drop table

-- DROP TABLE oicob_pjmovel."_invoices";

CREATE TABLE oicob_pjmovel."_invoices" (
	id int8 NOT NULL GENERATED ALWAYS AS IDENTITY,
	file_id int4 NOT NULL,
	field_key varchar(50) NULL,
	"name" varchar(100) NOT NULL,
	email varchar(100) NULL,
	cpf_cnpj varchar(14) NOT NULL,
	kind_person bpchar(1) NOT NULL,
	msisdn int8 NULL,
	yyyy_mm_ref bpchar(7) NOT NULL,
	maturity date NOT NULL,
	value numeric(12,2) NULL,
	payday date NULL,
	barcode varchar(60) NULL,
	reason_return bpchar(1) NULL,
	risk_description varchar(15) NULL,
	risk_number int4 NULL,
	"number" int8 NOT NULL DEFAULT 0,
	whatsapp date NULL,
	hello date NULL,
	group_control date NULL,
	blacklist date NULL,
	contributor date NULL,
	enrich_1 bool NOT NULL DEFAULT false,
	enrich_2 bool NOT NULL DEFAULT false,
	risk_description_b varchar(15) NULL,
	active bool NOT NULL,
	created_at timestamp NOT NULL DEFAULT now(),
	updated_at timestamp NOT NULL DEFAULT now(),
	arbor int8 NOT NULL DEFAULT 0,
	debit_number int8 NULL,
	CONSTRAINT "_invoices_pkey" PRIMARY KEY (id)
);
CREATE INDEX _invoices_active_idx ON oicob_pjmovel._invoices USING btree (active);
CREATE INDEX "_invoices_arbor_IDX" ON oicob_pjmovel._invoices USING btree (arbor);
CREATE INDEX "_invoices_contributor_IDX" ON oicob_pjmovel._invoices USING btree (contributor);
CREATE INDEX _invoices_cpf_cnpj_idx ON oicob_pjmovel._invoices USING btree (cpf_cnpj);
CREATE INDEX "_invoices_debit_number_IDX" ON oicob_pjmovel._invoices USING btree (debit_number);
CREATE INDEX _invoices_file_id_idx ON oicob_pjmovel._invoices USING btree (file_id);
CREATE INDEX "_invoices_group_control_IDX" ON oicob_pjmovel._invoices USING btree (group_control);
CREATE INDEX _invoices_kind_person_idx ON oicob_pjmovel._invoices USING btree (kind_person);
CREATE INDEX _invoices_maturity_idx ON oicob_pjmovel._invoices USING btree (maturity);
CREATE INDEX _invoices_msisdn_idx ON oicob_pjmovel._invoices USING btree (msisdn);
CREATE INDEX _invoices_number_idx ON oicob_pjmovel._invoices USING btree (number);
CREATE INDEX "_invoices_payday_IDX" ON oicob_pjmovel._invoices USING btree (payday);
CREATE INDEX "_invoices_risk_description_b_IDX" ON oicob_pjmovel._invoices USING btree (risk_description_b);
CREATE INDEX _invoices_risk_description_idx ON oicob_pjmovel._invoices USING btree (risk_description);
CREATE INDEX _invoices_risk_number_idx ON oicob_pjmovel._invoices USING btree (risk_number);
CREATE INDEX _invoices_yyyy_mm_ref_idx ON oicob_pjmovel._invoices USING btree (yyyy_mm_ref);

-- Drop table

-- DROP TABLE oicob_pjmovel.campaigns;

CREATE TABLE oicob_pjmovel.campaigns (
	id int4 NOT NULL GENERATED ALWAYS AS IDENTITY,
	"name" varchar(100) NOT NULL,
	product_id int4 NOT NULL,
	rule_id int4 NOT NULL,
	path_files_send varchar(100) NULL,
	path_files_return varchar(100) NULL,
	status bool NOT NULL DEFAULT false,
	provider_id int4 NOT NULL DEFAULT 1,
	sms bool NOT NULL DEFAULT false,
	isms bool NOT NULL DEFAULT false,
	voice bool NOT NULL DEFAULT true,
	voice_bot bool NOT NULL DEFAULT false,
	email bool NOT NULL DEFAULT false,
	whatsapp bool NOT NULL DEFAULT false,
	phone bool NOT NULL DEFAULT false,
	dialing_bot_provider varchar(5) NULL,
	dialing_provider varchar(5) NULL,
	created_at timestamp NOT NULL DEFAULT now(),
	updated_at timestamp NOT NULL DEFAULT now(),
	CONSTRAINT campaigns_pkey PRIMARY KEY (id)
);
CREATE INDEX campaigns_created_at_idx ON oicob_pjmovel.campaigns USING btree (created_at);
CREATE INDEX campaigns_name_idx ON oicob_pjmovel.campaigns USING btree (name);
CREATE INDEX campaigns_product_id_idx ON oicob_pjmovel.campaigns USING btree (product_id);
CREATE INDEX campaigns_provider_id_idx ON oicob_pjmovel.campaigns USING btree (provider_id);
CREATE INDEX campaigns_rule_id_idx ON oicob_pjmovel.campaigns USING btree (rule_id);
CREATE INDEX campaigns_voice_idx ON oicob_pjmovel.campaigns USING btree (voice);

-- Drop table

-- DROP TABLE oicob_pjmovel.eligibles;

CREATE TABLE oicob_pjmovel.eligibles (
	id int8 NOT NULL GENERATED ALWAYS AS IDENTITY,
	file_id int4 NOT NULL,
	field_key varchar(50) NULL,
	"name" varchar(100) NOT NULL,
	email varchar(100) NULL,
	cpf_cnpj varchar(14) NOT NULL,
	kind_person bpchar(1) NOT NULL,
	msisdn int8 NULL,
	yyyy_mm_ref bpchar(7) NOT NULL,
	maturity date NOT NULL,
	value numeric(12,2) NULL,
	payday date NULL,
	barcode varchar(60) NULL,
	reason_return bpchar(1) NULL,
	risk_description varchar(15) NULL,
	risk_number int4 NULL,
	"number" int8 NOT NULL DEFAULT 0,
	whatsapp date NULL,
	hello date NULL,
	group_control date NULL,
	blacklist date NULL,
	contributor date NULL,
	enrich_1 bool NOT NULL DEFAULT false,
	enrich_2 bool NOT NULL DEFAULT false,
	risk_description_b varchar(15) NULL,
	active bool NOT NULL,
	created_at timestamp NOT NULL DEFAULT now(),
	updated_at timestamp NOT NULL DEFAULT now(),
	arbor int8 NOT NULL DEFAULT 0,
	debit_number int8 NULL,
	CONSTRAINT eligibles_pkey PRIMARY KEY (id)
);
CREATE INDEX eligibles_active_idx ON oicob_pjmovel.eligibles USING btree (active);
CREATE INDEX "eligibles_arbor_IDX" ON oicob_pjmovel.eligibles USING btree (arbor);
CREATE INDEX "eligibles_contributor_IDX" ON oicob_pjmovel.eligibles USING btree (contributor);
CREATE INDEX eligibles_cpf_cnpj_idx ON oicob_pjmovel.eligibles USING btree (cpf_cnpj);
CREATE INDEX "eligibles_debit_number_IDX" ON oicob_pjmovel.eligibles USING btree (debit_number);
CREATE INDEX eligibles_file_id_idx ON oicob_pjmovel.eligibles USING btree (file_id);
CREATE INDEX "eligibles_group_control_IDX" ON oicob_pjmovel.eligibles USING btree (group_control);
CREATE INDEX eligibles_kind_person_idx ON oicob_pjmovel.eligibles USING btree (kind_person);
CREATE INDEX eligibles_maturity_idx ON oicob_pjmovel.eligibles USING btree (maturity);
CREATE INDEX eligibles_msisdn_idx ON oicob_pjmovel.eligibles USING btree (msisdn);
CREATE INDEX eligibles_number_idx ON oicob_pjmovel.eligibles USING btree (number);
CREATE INDEX "eligibles_payday_IDX" ON oicob_pjmovel.eligibles USING btree (payday);
CREATE INDEX "eligibles_risk_description_b_IDX" ON oicob_pjmovel.eligibles USING btree (risk_description_b);
CREATE INDEX eligibles_risk_description_idx ON oicob_pjmovel.eligibles USING btree (risk_description);
CREATE INDEX eligibles_risk_number_idx ON oicob_pjmovel.eligibles USING btree (risk_number);
CREATE INDEX eligibles_yyyy_mm_ref_idx ON oicob_pjmovel.eligibles USING btree (yyyy_mm_ref);

-- Drop table

-- DROP TABLE oicob_pjmovel.files;

CREATE TABLE oicob_pjmovel.files (
	id int4 NOT NULL GENERATED ALWAYS AS IDENTITY,
	product_id int4 NOT NULL,
	file_number int8 NOT NULL,
	file_date date NOT NULL,
	file_name varchar(100) NOT NULL,
	lines int4 NOT NULL DEFAULT 0,
	processed bool NOT NULL DEFAULT false,
	created_at timestamp NOT NULL DEFAULT now(),
	updated_at timestamp NOT NULL DEFAULT now(),
	CONSTRAINT files_pkey PRIMARY KEY (id)
);
CREATE INDEX files_file_date_idx ON oicob_pjmovel.files USING btree (file_date);
CREATE INDEX files_file_name_idx ON oicob_pjmovel.files USING btree (file_name);
CREATE INDEX files_file_number_idx ON oicob_pjmovel.files USING btree (file_number);
CREATE INDEX files_file_type_id_idx ON oicob_pjmovel.files USING btree (product_id);
CREATE INDEX files_processed_idx ON oicob_pjmovel.files USING btree (processed);
CREATE INDEX files_product_id_idx ON oicob_pjmovel.files USING btree (product_id);

-- Drop table

-- DROP TABLE oicob_pjmovel.payments;

CREATE TABLE oicob_pjmovel.payments (
	id int8 NOT NULL GENERATED ALWAYS AS IDENTITY,
	file_id int4 NOT NULL,
	fieldkey varchar(50) NOT NULL,
	msisdn int8 NULL,
	date_ref date NOT NULL,
	reason varchar(5) NULL,
	created_at timestamp NOT NULL DEFAULT now(),
	CONSTRAINT payments_pkey PRIMARY KEY (id)
);
CREATE INDEX payments_date_ref_idx ON oicob_pjmovel.payments USING btree (date_ref);
CREATE INDEX payments_msisdn_idx ON oicob_pjmovel.payments USING btree (msisdn);
CREATE INDEX payments_reason_idx ON oicob_pjmovel.payments USING btree (reason);

-- Drop table

-- DROP TABLE oicob_pjmovel.rules;

CREATE TABLE oicob_pjmovel.rules (
	id int4 NOT NULL GENERATED ALWAYS AS IDENTITY,
	"rule" int4 NOT NULL,
	range_min int4 NOT NULL,
	range_max int4 NOT NULL,
	risk int4 NULL,
	score int4 NULL,
	region bpchar(2) NULL,
	status bool NOT NULL DEFAULT false,
	product_id int4 NOT NULL,
	risk_description varchar(15) NULL,
	risk_signal varchar(5) NULL,
	enrich bool NOT NULL DEFAULT false,
	conditions varchar(4000) NULL,
	prefix_file varchar(50) NULL,
	min_total_debt numeric(12,2) NULL,
	created_at timestamp NOT NULL DEFAULT now(),
	updated_at timestamp NOT NULL DEFAULT now(),
	sms bool NULL DEFAULT false,
	isms bool NULL DEFAULT false,
	voicer bool NULL DEFAULT false,
	voicerbot bool NULL DEFAULT false,
	email bool NULL DEFAULT false,
	range_group int4 NULL DEFAULT 0,
	range_rest int4 NULL DEFAULT 0,
	CONSTRAINT rules_pkey PRIMARY KEY (id)
);
CREATE INDEX rules_created_at_idx ON oicob_pjmovel.rules USING btree (created_at);
CREATE INDEX rules_product_id_idx ON oicob_pjmovel.rules USING btree (product_id);
CREATE INDEX rules_range_max_idx ON oicob_pjmovel.rules USING btree (range_max);
CREATE INDEX rules_range_min_idx ON oicob_pjmovel.rules USING btree (range_min);
CREATE INDEX rules_region_idx ON oicob_pjmovel.rules USING btree (region);
CREATE INDEX rules_risk_idx ON oicob_pjmovel.rules USING btree (risk);
CREATE INDEX rules_rule_idx ON oicob_pjmovel.rules USING btree (rule);
CREATE INDEX rules_score_idx ON oicob_pjmovel.rules USING btree (score);

ALTER TABLE oicob_pjmovel."_invoice_payments" ADD CONSTRAINT "_invoice_payments_file_id_fkey" FOREIGN KEY (file_id) REFERENCES oicob_pjmovel.files(id);

ALTER TABLE oicob_pjmovel."_invoice_phones" ADD CONSTRAINT "_invoice_phones_file_id_fkey" FOREIGN KEY (file_id) REFERENCES oicob_pjmovel.files(id);
ALTER TABLE oicob_pjmovel."_invoice_phones" ADD CONSTRAINT "_invoice_phones_invoice_id_fkey" FOREIGN KEY (invoice_id) REFERENCES oicob_pjmovel._invoices(id) ON DELETE CASCADE;

ALTER TABLE oicob_pjmovel."_invoices" ADD CONSTRAINT "_invoices_file_id_fkey" FOREIGN KEY (file_id) REFERENCES oicob_pjmovel.files(id);

ALTER TABLE oicob_pjmovel.campaigns ADD CONSTRAINT campaigns_product_id_fkey FOREIGN KEY (product_id) REFERENCES products(id);
ALTER TABLE oicob_pjmovel.campaigns ADD CONSTRAINT campaigns_rule_id_fkey FOREIGN KEY (rule_id) REFERENCES oicob_pjmovel.rules(id);

ALTER TABLE oicob_pjmovel.eligibles ADD CONSTRAINT eligibles_file_id_fkey FOREIGN KEY (file_id) REFERENCES oicob_pjmovel.files(id);

ALTER TABLE oicob_pjmovel.files ADD CONSTRAINT files_product_id_fkey FOREIGN KEY (product_id) REFERENCES products(id);

ALTER TABLE oicob_pjmovel.payments ADD CONSTRAINT payments_file_id_fkey FOREIGN KEY (file_id) REFERENCES oicob_pjmovel.files(id);

ALTER TABLE oicob_pjmovel.rules ADD CONSTRAINT rules_product_id_fkey FOREIGN KEY (product_id) REFERENCES products(id);
