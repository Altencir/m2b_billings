#!/bin/bash
#############################################################################
#     Date: 18/09/2019 16:45                                                #
#   Author: ALTencir Silva                                                  #
#    About: import of payments OI COB PF FIBRA                              #
#############################################################################

# loading environment variables #
source ~/.bash_profile

# path #
_PATH="/var/www/m2b_billings/oi/pf/fibra/"

# acessing #
cd ~/var/www/m2b_billings/oi/pf/fibra/

# setting variables #
_days=0
_year4digit=`date +%Y -d "$_days day ago"`
_year2digit=`date +%y -d "$_days day ago"`
_month=`date +%m -d "$_days day ago"`
_day=`date +%d -d "$_days day ago"`
_is_mailing="false"
_product_id=12
_date=`date +%Y-%m-%d -d "$_days day ago"`
_filedate=`date +%Y-%m-%d -d "$_days day ago"`

# downloading files #
echo Download ...
wget --ftp-user=$FTP_USER --ftp-password=$FTP_PASSWD ftp://$FTP_HOST/CONTAX/Keyrus/Moby2Buy/'Retencao Invol'/Arquivos/Movel/ICS/*
wget --ftp-user=$FTP_USER --ftp-password=$FTP_PASSWD ftp://$FTP_HOST/CONTAX/Keyrus/Moby2Buy/'Retencao Invol'/Arquivos/Movel/ICS/processados/*

# reading full folder #
for _filename in `find ~/smartware-laravel/scripts/m2b_billing/oi/pf/fibra/*.agc -type f`
do

	# criando arquivo temporário
	FILE="file.asc"

	# limpando TABULAÇÃO
	echo 'limpando TABULAÇÃO e "\"'
	sed -i "s/\\\\/\//g" $_filename
	sed -e "s/[[:space:]]\+/ /g" $_filename > $FILE
	
	# limpando PLICAS (aspas simples)
	echo 'limpando PLICAS (aspas simples)'
	sed "s/'//g" $FILE > $_filename
	
	# convertendo para utf8 se precisar
	echo 'convertendo para utf8 se precisar'

	ISO_YES=$(file "$_filename"|grep '8859'|wc -l)
	if [ "$ISO_YES" -eq 1 ]; then
        echo "iso-8859 detectado em $_filename"
        iconv -f iso-8859-1 -t utf-8 "${_filename}" > "${_filename}.new" && echo -e "Arquivo ${_filename} convertido com sucesso\n"
        cp "$_filename" "${_filename}.bkp"
        mv "${_filename}.new" "${_filename}"
	fi

	ISO_YES=$(file "$_filename"|grep 'Non-ISO'|wc -l)
	if [ "$ISO_YES" -eq 1 ]; then
        echo "iso-8859 detectado em $_filename"
        iconv -f iso-8859-1 -t utf-8 "${_filename}" > "${_filename}.new" && echo -e "Arquivo ${_filename} convertido com sucesso\n"
        cp "$_filename" "${_filename}.bkp"
        mv "${_filename}.new" "${_filename}"
	fi

    # setting enconding #
    iconv -c -f utf-8 -t ascii $_filename > "$_filename".tmp
    mv -f "$_filename".tmp "$_filename"
    rm -f "$_filename".tmp
    rm -f "${_filename}.bkp"

    # reading file number of file name #
    _header=$(head -n1 $_filename)
    _filenumber=${_header:4:6}

    # reading file name #
    _file_name=`echo $_filename | cut -d "/" -f 10`
    _filedate=`echo $_file_name | cut -d "_" -f 7`

    # find filename in files #
    _file_id=`psql -t -U postgres -d m2b -c "select id from oicob_pffibra.files where file_name='$_file_name';" | tr -d ' '`

    # find last date mailing #
    _mailing_date=`psql -t -U postgres -d m2b -c "select file_number from oicob_pffibra.files where processed is true order by file_date limit 1;" | tr -d ' '`
    _mailing_date=`echo $((_mailing_date))`
    _mailing_date=`echo $((_filedate >= _mailing_date))`
    
    echo "$_mailing_date"

    # found record in <files> #
    if [ "${_mailing_date}" -eq 1 ];
    then 
        if [ "$_file_id" = "NULL" ] || [ "$_file_id" = "null" ] || [ "$_file_id" = "" ];
        then

            # displaying file name #
            echo "processing: [ ${_file_name} ] [ file number: $_filenumber ]..."

            # reading the lines count #
            _num_of_lines=$(cat "$_filename" | wc -l)

            # adding new record in <files> #
            _file_id=`psql -t -q -U postgres -d m2b -c "insert into oicob_pffibra.files(product_id,file_number,file_date,file_name,lines,created_at)values($_product_id,$_filenumber,cast('$_filedate' as date),'$_file_name',$_num_of_lines,'$_date') returning id;" | tr -d ' '`

            # process payments #
            psql -U postgres -d m2b -c "truncate table oicob_pffibra._tmp_;"
            psql -U postgres -d m2b -c "\copy oicob_pffibra._tmp_ from '$_filename';"
            psql -U postgres -d m2b -c "select * from oicob_pffibra.invoices_sp($_is_mailing, $_file_id, $_filenumber, $_num_of_lines)"

        else

            # displaying file name #
            echo "file name: [ ${_file_name} ] [ file number: $_filenumber ] had previously been processed..."

        fi
    fi

    # removing file #
    rm $_filename

done
rm file.asc

echo "Done!"

