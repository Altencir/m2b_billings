#!/bin/bash
#############################################################################
#     Date: 26/09/2019 11:29                                                #
#   Author: ALTencir Silva                                                  #
#    About: import of mailings OI COB PF FIXOR2                             #
#############################################################################

# loading environment variables #
source ~/.bash_profile

# path #
_PATH="/var/www/m2b_billings/oi/pf/fixor2/"

# acessing #
cd ~/var/www/m2b_billings/oi/pf/fixor2/

# initializing variables #
_days=3
_day=$(date +"%a" -d "$_days day ago")
_is_mailing="true"
_product_id=10
_prefix_file="NOVO_MAILING_COB_FIXO_R2_"
_date=`date +%Y-%m-%d -d "0 day ago"`
_filedate=`date +%Y-%m-%d -d "$_days day ago"`
_filenumber=`date +%Y%m%d -d "$_days day ago"`
_filename_zip="${_prefix_file}${_filenumber}.zip"
_filename="${_prefix_file}${_filenumber}.txt"
_filename_save="${_filename}.sav"

# find filename in files #
_file_id=`psql -t -U postgres -d m2b -c "select id from oicob_pffixor2.files where file_name='$_filename';" | tr -d ' '`

# found record in <files> #
if [ "$_file_id" != "NULL" ] && [ "$_file_id" != "null" ] && [ "$_file_id" != "" ];
then
        # already been imported #
        echo "Arquivo [$_filename_zip] já foi importado!"
        exit
fi

# download mailing #
wget --ftp-user=$FTP_USER_R1_PF --ftp-password=$FTP_PASSWD_R1_PF ftp://$FTP_HOST_R1_PF/1_Envio/COBRANCA/FIXO/R2/${_filename_zip}

# file not exists .zip? #
if [ ! -f "$_filename_zip" ];
then
        echo "Arquivo [$_filename_zip] ainda não disponibilizado no FTP."
        exit
fi

# unziping #
unzip ${_filename_zip}

# file not exists .txt? #
if [ ! -f "$_filename" ];
then
        echo "Erro na operação de unzip. Arquivo [$_filename] não encontrado!"
        exit
fi

# setting enconding #
iconv -c -f utf-8 -t ascii $_filename > "$_filename".tmp
mv -f "$_filename".tmp "$_filename"
rm -f "$_filename".tmp

# setting return carriage
sed -i 's/\r//g' $_filename

# replacing tab with space in $_filename
sed -i $'s/\t/ /g' $_filename

# getting the lines count #
_num_of_lines=$(cat "$_filename" | wc -l)

# found record in <files> #
if [ "$_file_id" = "NULL" ] || [ "$_file_id" = "null" ] || [ "$_file_id" = "" ];
then
        # adding new record in <files> #
        _file_id=`psql -t -q -U postgres -d m2b -c "insert into oicob_pffixor2.files(product_id,file_number,file_date,file_name,lines,created_at, processed)values($_product_id,$_filenumber,'$_filedate','$_filename',$_num_of_lines,'$_date','TRUE') returning id;" | tr -d ' '`

        # adding new column in mailing csv [file_id] #
        awk -v d="$_file_id" -F";" 'BEGIN {OFS = ";"} FNR==1{$(NF+1)="file_id"} FNR>1{$(NF+1)=d;} 1' $_filename > $_filename_save 

        # adding tmp #
        psql -U postgres -d m2b -c "truncate table oicob_pffixor2._tmp_;"
        psql -U postgres -d m2b -c "\copy oicob_pffixor2._tmp_ from '$_filename_save';"

        # if not error #
        if [ $? -eq 0 ]; then

                # preparing table #
                psql -U postgres -d m2b -c "TRUNCATE TABLE oicob_pffixor2._invoices RESTART IDENTITY CASCADE;"
                psql -U postgres -d m2b -c "TRUNCATE TABLE oicob_pffixor2._invoice_payments RESTART IDENTITY CASCADE;"

                # saving <invoices> #
                echo "saving files [invoices / phones]..."
                _error=`psql -t -U postgres -d m2b -c "select * from oicob_pffixor2.invoices_sp($_is_mailing, $_file_id, $_filenumber, $_num_of_lines);" | tr -d ' '`

                # removing file #
                rm $_filename_save

                # update RISK #
                psql -U postgres -d m2b -c "select * from oicob_pffixor2.update_risk_sp();"
                #
        else
                # removing file #
                rm $_filename_save

                # error import #
                echo "error importing..."
                # cat email.env | mail -s "Cobrança OI-PF Fixor2: <Erro na importação [Encoding] do Mailing>" altencir@mobi2buy.com, squad.cobranca@mobi2buy.com

                # rollback #
                psql -U postgres -d m2b -c "delete from oicob_pffixor2.files where id=$_file_id;"
                #
        fi 

else
        # has imported #
        echo "Arquivo [$_filename_zip] já foi importado!"
fi

# removing file #
rm $_filename_zip
rm $_filename

echo "Done!"

