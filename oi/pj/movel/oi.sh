#!/bin/bash
#############################################################################
#     Date: 27/08/2019 18:05                                                #
#   Author: ALTencir Silva                                                  #
#    About: importing Velip files                                           #
#############################################################################

# loading environment variables #
source ~/.bash_profile

# path #
_PATH="/var/www/m2b_billings/oi/pj/movel/"

# acessing #
cd /var/www/m2b_billings/oi/pj/movel/

# processing VELIP returns #############################################################
echo "importing VELIP returns..."
bash ~/m2b_billings/oi/pj/movel/velip.sh

# importing #############################################################
echo "importing mailing..."
bash ~/m2b_billings/oi/pj/movel/import.sh

# payments #############################################################
echo "importing payments..."
bash ~/m2b_billings/oi/pj/movel/payments.sh

# processing eligibles #############################################################
echo "processing eligibles..."
psql -U postgres -d m2b -c "select * from oicob_pjmovel.eligibles_sp();"

# sending to s3 #############################################################
echo "processing..."
# psql -t -U postgres -d m2b -c "\copy (select _sms,_isms,_voicer,_voicerbot,_email,velip_file,velip_nome,velip_numero,velip_numero2,velip_numero3,velip_extra1,velip_extra2,velip_extra3,velip_extra4,velip_extra5,velip_extra6,velip_extra7,velip_extra8,velip_cod_cli,msisdn,delay,email from oicob_pjmovelpos.eligibles) To 'bom_eligibles_smartware.csv' With CSV HEADER DELIMITER ';';"

##################################################################:> Uploading to AWS
echo "sending s3..."
# aws s3 cp bom_eligibles_smartware.csv s3://m2b-cobranca/velip/eligibles/
########################################################################

echo "sending email..."
# cat text.txt | mail -s "ATENÇÃO! O arquivo de Cobrança Movel PJ [movel_pj_eligibles_smartware.csv] foi enviado para o S3." squad.cobranca@mobi2buy.com

# end #
echo "Done."
