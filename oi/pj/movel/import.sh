#!/bin/bash
#############################################################################
#     Date: 11/09/2019 11:35                                                #
#   Author: ALTencir Silva                                                  #
#    About: import of mailings OI COB PJ MOVEL                              #
#############################################################################

# loading environment variables #
source ~/.bash_profile

# path #
_PATH="/var/www/m2b_billings/oi/pj/movel/"

# acessing #
cd ~/var/www/m2b_billings/oi/pj/movel/

# should it run? #
_days=3
_day=$(date +"%a" -d "$_days day ago")

if [ "$_day" != "Wed" ] && [ "$_day" != "Fri" ]; 
then   
        echo "Hoje não é dia de mailing [Cobrança OI-PJ MOVEL]."
        exit
fi

# initializing variables #
_days=4
_is_mailing="true"
_product_id=7
_prefix_file="NOVO_MAILING_COBRANCA_MOVEL_EMP_"
_date=`date +%Y-%m-%d -d "0 day ago"`
_filedate=`date +%Y-%m-%d -d "$_days day ago"`
_filenumber=`date +%Y%m%d -d "$_days day ago"`
_filename_zip="${_prefix_file}${_filenumber}.zip"
_filename="${_prefix_file}${_filenumber}.TXT"
_filename_save="${_filename}.sav"

# find filename in files #
_file_id=`psql -t -U postgres -d m2b -c "select id from oicob_pjmovel.files where file_name='$_filename';" | tr -d ' '`

# found record in <files> #
if [ "$_file_id" != "NULL" ] && [ "$_file_id" != "null" ] && [ "$_file_id" != "" ];
then
        # already been imported #
        echo "Arquivo [$_filename_zip] já foi importado!"
        exit
fi

# download mailing #
wget --ftp-user=$FTP_USER_R1_PF --ftp-password=$FTP_PASSWD_R1_PF ftp://$FTP_HOST_R1_PF/1_Envio/COBRANCA/MOVEL/${_filename_zip}

# file not exists .zip? #
if [ ! -f "$_filename_zip" ];
then
        echo "Arquivo [$_filename_zip] ainda não disponibilizado no FTP."
        exit
fi

##################################################################:> Uploading to AWS
# aws s3 cp ${_filename_zip} s3://m2b-cobranca/oi/pf/movel/mailings/
##################################################################

# unziping #
unzip ${_filename_zip}

# file not exists .txt? #
if [ ! -f "$_filename" ];
then
        echo "Erro na operação de unzip. Arquivo [$_filename] não encontrado!"
        exit
fi

# setting enconding #
iconv -c -f utf-8 -t ascii $_filename > "$_filename".tmp
mv -f "$_filename".tmp "$_filename"
rm -f "$_filename".tmp

# setting return carriage
sed -i 's/\r//g' $_filename

# getting the lines count #
_num_of_lines=$(cat "$_filename" | wc -l)

# found record in <files> #
if [ "$_file_id" = "NULL" ] || [ "$_file_id" = "null" ] || [ "$_file_id" = "" ];
then
        # adding new record in <files> #
        _file_id=`psql -t -q -U postgres -d m2b -c "insert into oicob_pjmovel.files(product_id,file_number,file_date,file_name,lines,created_at, processed)values($_product_id,$_filenumber,'$_filedate','$_filename',$_num_of_lines,'$_date','TRUE') returning id;" | tr -d ' '`

        # adding new column in mailing csv [file_id] #
        awk -v d="$_file_id" -F";" 'BEGIN {OFS = ";"} FNR==1{$(NF+1)="file_id"} FNR>1{$(NF+1)=d;} 1' $_filename > $_filename_save 

        # adding tmp #
        psql -U postgres -d m2b -c "truncate table oicob_pjmovel._tmp_;"
        psql -U postgres -d m2b -c "\copy oicob_pjmovel._tmp_ from '$_filename_save';"

        # if not error #
        if [ $? -eq 0 ]; then

                # preparing table #
                psql -U postgres -d m2b -c "TRUNCATE TABLE oicob_pjmovel._invoices RESTART IDENTITY CASCADE;"
                psql -U postgres -d m2b -c "TRUNCATE TABLE oicob_pjmovel._invoice_payments RESTART IDENTITY CASCADE;"

                # saving <invoices> #
                echo "saving files [invoices / phones]..."
                _error=`psql -t -U postgres -d m2b -c "select * from oicob_pjmovel.invoices_sp($_is_mailing, $_file_id, $_filenumber, $_num_of_lines);" | tr -d ' '`

                # removing file #
                rm $_filename_save

                # update RISK #
                psql -U postgres -d m2b -c "select * from oicob_pjmovel.update_risk_sp();"
                #
        else
                # removing file #
                rm $_filename_save

                # error import #
                echo "error importing..."
                cat desktop.ini | mail -s "Cobrança OI-PJ MOVEL: <Erro na importação [Encoding] do Mailing>" squad.cobranca@mobi2buy.com

                # rollback #
                psql -U postgres -d m2b -c "delete from oicob_pjmovel.files where id=$_file_id;"
                #
        fi 

else
        # has imported #
        echo "Arquivo [$_filename_zip] já foi importado!"
fi

# removing file #
rm $_filename_zip
rm $_filename

echo "Done!"

